﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
//using Android.Content.PM;

using Xamarin.Forms;
using MonTechApp.Interface;
using MonTechApp.Commons;
using TCLLIB.Sensors;
using Android.Content.PM;
//using MonTechApp.Helper;
//using System.Security.AccessControl;
//using System.Runtime.CompilerServices;
//using Org.Apache.Http.Impl.Client;

[assembly: Xamarin.Forms.Dependency(typeof(MonTechApp.Droid.PlatformFile))]
namespace MonTechApp.Droid
{
    public class PlatformFile : IPlatformFile
    {

        //ファイル出力先のルート（外部ストレージ）を取得　デバイス依存
        public string GetRootFolder()
        {
            string result = "";
            try
            {
                //ストレージへのアクセス許可の確認
                RequestPermission();

                var exStorageDir = Android.OS.Environment.ExternalStorageDirectory;
                var exStorageStatus = Android.OS.Environment.ExternalStorageState;
                result = exStorageDir.Path;
            }
            catch (Exception ex)
            {
                throw new Exception("Exception in GetRootFolder", ex);
            }
            return result;
        }

        //ファイル出力先のを取得
        public string GetOutPutFolder()
        {
            string result = "";
            try
            {
                var exStorageDir = Android.OS.Environment.ExternalStorageDirectory;
                result = System.IO.Path.Combine(exStorageDir.Path, ShareSetting.subFolderName);
                //result = exStorageDir.Path;
            }
            catch (Exception ex)
            {
                throw new Exception("Exception in GetOutPutFolder", ex);
            }
            return result;
        }

        //ファイルの作成　最初の出力のみ
        public async Task<bool> CreateFile(string filename)
        {
            bool result = false;
            //Task<bool> result;
            try
            {
                //string folder = GetOutPutFolder();
                string path = System.IO.Path.Combine(GetOutPutFolder(), filename);

                if (!File.Exists(path))
                {
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.WriteLine(FileHeader.HeaderLine());
                    }
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception in CreateFile", ex);
            }
            return await Task.FromResult(result);
        }

        //ファイルに1行追記
        public async Task<bool> AppendLine(string filename, string text)
        {
            bool result = false;
            try
            {
                RequestPermission();

                string outPutFolder = GetOutPutFolder();
                //string subFolder = string.Format("{0}/{1}", folder, "clmdata");
                //string subFolder = System.IO.Path.Combine(folder, "clmdata");

                //フォルダの有無をチェック
                if (!Directory.Exists(outPutFolder))
                {
                    Directory.CreateDirectory(outPutFolder);
                    //var sec = Directory.GetAccessControl(folder);
                    //sec.AddAccessRule(new FileSystemAccessRule(
                    //    "everyone",
                    //    FileSystemRights.Modify,
                    //    InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                    //    PropagationFlags.None,
                    //    AccessControlType.Allow));
                    //Directory.SetAccessControl(folder, sec);
                }

                string FilePath = System.IO.Path.Combine(outPutFolder, filename);
                //File.AppendAllText(path, "Hello World\n");
                using (StreamWriter sw = File.AppendText(FilePath))
                {
                    sw.WriteLine(text);
                }

                result = true;
            }
            catch (Exception ex)
            {

            }
            return await Task.FromResult(result);
        }

         //ファイルに登山データを出力
        public async Task<bool> AppendClimbingData(string filename, ClimbingData value)
        {
            bool result = false;
            try
            {
                string folder = GetOutPutFolder();
                string path = System.IO.Path.Combine(folder, filename);
                if (!File.Exists(filename))
                {
                    //create file within headers
                    using (StreamWriter sw = File.AppendText(path))
                    {
                        var strArray = new[] {
                            value.mtid.ToString(),
                            value.climbno.ToString(),
                            value.rcd_tm.ToString(),
                            value.gpslat.ToString(),
                            value.gpslon.ToString(),
                            value.gpsalt.ToString(),
                            value.wgsalt.ToString(),
                            value.gpsacc.ToString(),
                            value.gpsrgb.ToString(),
                            value.presnow.ToString(),
                            value.pressea.ToString(),
                            value.presalt.ToString(),
                            value.tilex.ToString(),
                            value.tiley.ToString(),
                            value.tilezoom.ToString(),
                            value.dst_vrt.ToString(),
                            value.dst_hrt.ToString(),
                            value.spd_vrt.ToString(),
                            value.ergcsn_rt.ToString(),
                            value.mets_sp.ToString(),
                            value.ttl_mvsec.ToString(),
                            value.ttl_mvdst.ToString(),
                            value.ttl_up_mvdst.ToString(),
                            value.ttl_dw_mvdst.ToString(),
                            value.ttl_ergcsn.ToString(),
                            value.mets_ave.ToString(),
                            value.psn_wgt.ToString(),
                            value.psn_lug.ToString(),
                            value.gpslat_crt.ToString(),
                            value.gpslon_crt.ToString(),
                            value.gpsalt_crt.ToString(),
                            value.wgsalt_crt.ToString(),
                            value.presnow_crt.ToString(),
                        };
                        string strAddData = string.Join(",", strArray);
                        sw.WriteLine(strAddData);
                    }
                    result = true;
                }
            }catch(Exception ex)
            {
                throw new Exception("Exception in AppendClimbingData", ex);

            }
            return await Task.FromResult(result);
        }


        //下記サイトを参考にした
        //　Android6 Marshmallow以降のパーミッションについて | Xamarin.Forms
        //  https://itblogdsi.blog.fc2.com/blog-entry-258.html

        //パミッション確認のポップアップ
        public void RequestPermission()
        {
            const int PermissionCode = 1001;

            //Android6 Marshmallow以降
            if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
            {
                string[] permissions = new string[] { Android.Manifest.Permission.WriteExternalStorage,
                                              Android.Manifest.Permission.ReadExternalStorage };

                Context context = Forms.Context;

                foreach (string pm in permissions)
                {
                    if (context.CheckSelfPermission(pm) != Permission.Granted)
                    {
                        //許可を求める
                        ((Activity)context).RequestPermissions(permissions, PermissionCode);
                        break;
                    }
                }
            }

        }


    }

    //public static class RequestCode
    //{
    //    public const int Permission = 1001;
    //}


}