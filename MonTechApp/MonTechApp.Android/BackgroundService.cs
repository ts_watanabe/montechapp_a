﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;

using TCLLIB.Sensors;
using MonTechApp.Helper;

namespace MonTechApp.Droid
{
    [Service(Name = "MonTechApp.Android.BackgroundService", Process =":SensorMonitorProcess")]
    public class BackgroundService : Service
    {
        SensorHelper sensors;
        
        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                this.RegisterForegroundService();
            }
            Task task = new Task(() =>
            {
                sensors = new SensorHelper();
            });
            task.Start();
            return base.OnStartCommand(intent, flags, startId);
        }
        void RegisterForegroundService()
        {
            //画面から呼び出す場合
            //Activity activity = (Activity)(Forms.Context);

            //バックグラウンドから呼び出す場合
            Activity activity = (Activity)(Android.App.Application.Context);


            //NotificationManager manager = (NotificationManager)context.GetSystemService(Context.NotificationService);
            NotificationManager manager = (NotificationManager)activity.GetSystemService(Context.NotificationService);

            //Andorid8.0 Oreo 以降の通知で必要なChannel
            string channelId = "任意のID";
            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                string channelNm = "任意の名称"; //Androidのアプリ設定画面に表示されます。
                var importance = NotificationManager.ImportanceHigh;
                NotificationChannel channel = new NotificationChannel(channelId, channelNm, importance);
                channel.Description = "任意の説明";
                manager.CreateNotificationChannel(channel);
            }
            var notification = new Notification.Builder(this)
                    //.SetContentTitle(Resources.GetString(Resource.String.app_name))
                    .SetContentText("Started ForegroundService.")
                    //.SetSmallIcon(Resource.Drawable.icon_alpha) //Android7.0対応
                    //.SetColor(ActivityCompat.GetColor(Android.App.Application.Context, Resource.Color.notification_color))  //Android7.0対応
                    .SetOngoing(true)
                    .SetChannelId(channelId)  //Android8.0対応
                    .Build();

            // Enlist this instance of the service as a foreground service
            this.StartForeground(99999, notification);    //IDの値は任意
        }
        public void StartBackgroundService()
        {
            //start service
            Intent serviceIntent = new Intent(this, typeof(BackgroundService));
            serviceIntent.AddFlags(ActivityFlags.NewTask);
            serviceIntent.SetPackage(this.PackageManager.GetPackageInfo(this.PackageName, 0).PackageName);
            base.StartService(serviceIntent);
        }
        public override void OnDestroy()
        {
            base.OnDestroy();

            //restart service
            this.StartBackgroundService();
        }
    }
}