﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MonTechApp.Interface;

[assembly: Xamarin.Forms.Dependency(typeof(MonTechApp.Droid.ServiceGeoLocator))]

namespace MonTechApp.Droid
{
    public class ServiceGeoLocator : IGeoLocator
    {
        public event LocationEventHandler LocationReceived;

        [Obsolete]
        public void StartGps()
        {
            RequestPermission();

            var context = Xamarin.Forms.Forms.Context;
            var locationMan = context.GetSystemService(Context.LocationService) as LocationManager;

            locationMan.RequestLocationUpdates(LocationManager.GpsProvider, 0, 0,
                new MyLocationListener(l =>
                {
                    System.Diagnostics.Debug.WriteLine(string.Format("*Location Update {0} ( {1} / {2} )", DateTime.Now.ToString("hh:mm:ss.fff"), l.Latitude, l.Longitude));
                    if (this.LocationReceived != null)
                    {
                        this.LocationReceived(this, new LocationEventArgs
                        {
                            Latitude = l.Latitude,
                            Longitude = l.Longitude,
                            Accuracy = l.Accuracy,
                            Altitude = l.Altitude
                        });
                    }
                }));
            System.Diagnostics.Debug.WriteLine("*Start GPS Listener for Android");

        }

        [Obsolete]
        public void RequestPermission()
        {
            const int PermissionCode = 1008;

            //Android6 Marshmallow以降
            if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
            {
                string[] permissions = new string[] {
                    Android.Manifest.Permission.AccessCoarseLocation,
                    Android.Manifest.Permission.AccessFineLocation
                };

                Context context = Xamarin.Forms.Forms.Context;

                foreach (string pm in permissions)
                {
                    if (context.CheckSelfPermission(pm) != Android.Content.PM.Permission.Granted)
                    {
                        //許可を求める
                        ((Activity)context).RequestPermissions(permissions, PermissionCode);
                        break;
                    }
                }
            }

        }


        class MyLocationListener : Java.Lang.Object, ILocationListener
        {
            private readonly Action<Location> _onLocationChanged;

            public MyLocationListener(Action<Location> onLocationChanged)
            {
                _onLocationChanged = onLocationChanged;
            }

            public void OnLocationChanged(Location location)
            {
                _onLocationChanged(location);
            }

            public void OnProviderDisabled(string provider) 
            {
                System.Diagnostics.Debug.WriteLine(string.Format("* Disabled Provider : {0}", provider));
            }
            public void OnProviderEnabled(string provider) 
            {
                System.Diagnostics.Debug.WriteLine(string.Format("* Enabled Provider : {0}", provider));
            }
            public void OnStatusChanged(string provider, Availability status, Bundle extras) 
            {
                System.Diagnostics.Debug.WriteLine(string.Format("* Status : provider={0}, status={1}", provider, status.ToString()));
            }
        }
    }
}