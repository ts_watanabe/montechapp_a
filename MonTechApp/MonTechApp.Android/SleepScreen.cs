﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MonTechApp.Interface;

[assembly: Xamarin.Forms.Dependency(typeof(MonTechApp.Droid.SleepScreen))]


namespace MonTechApp.Droid
{
    public class SleepScreen : ISleepControl
    {
        PowerManager.WakeLock wl;

        [Obsolete]
        public void SleepDisabled()
        {
            if (wl != null) return;
            PowerManager pm = (PowerManager)(Xamarin.Forms.Forms.Context.GetSystemService(Context.PowerService));
            Context context = Xamarin.Forms.Forms.Context;
            var name = context.PackageManager.GetPackageInfo(context.PackageName, 0).PackageName;

            if (!pm.IsIgnoringBatteryOptimizations(name))
            {
                //Intent intent = new Intent(context, typeof(SleepScreen));
                Intent intent = new Intent(Android.Provider.Settings.ActionRequestIgnoreBatteryOptimizations);
                
                intent.SetData(Android.Net.Uri.Parse("package:" + name));
                //intent.SetAction(Android.Provider.Settings.ActionRequestIgnoreBatteryOptimizations);
                

                context.StartActivity(intent);
            }

            wl = pm.NewWakeLock(WakeLockFlags.ScreenBright | WakeLockFlags.OnAfterRelease, "Sleep Now");
            wl.Acquire();
        }

        public void SleepEnabled()
        {
            if (wl != null)
            {
                wl.Release();
                wl = null;
            }
        }
    }
}