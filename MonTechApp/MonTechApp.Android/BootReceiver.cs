﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MonTechApp.Droid
{
    [BroadcastReceiver]
    [IntentFilter(new[] { 
        Intent.ActionBootCompleted, 
        "android.intent.action.QUICKBOOT_POWERON",
        "android.intent.action.QUICKBOOT_POWERON",
        "android.intent.action.PACKAGE_INSTALL",
        "android.intent.action.PACKAGE_ADDED",
        Intent.ActionMyPackageReplaced
    })]
    public class BootReceiver
    {
    }
}