﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MonTechApp.Interface;
using Xamarin.Forms;

//[assembly: Dependency(typeof(PermissionService))]
[assembly: Xamarin.Forms.Dependency(typeof(MonTechApp.Droid.PermissionServiceAndroid))]
namespace MonTechApp.Droid
{
    public class PermissionServiceAndroid : IPermissionService
    {
        [Obsolete]
        public bool ExtraStragePermission()
        {
            bool result = false;
            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                Context context = Xamarin.Forms.Forms.Context;
                if (context.CheckSelfPermission(Android.Manifest.Permission.WriteExternalStorage) == Permission.Granted)
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        [Obsolete]
        public bool AccessFineLocationPermission()
        {
            bool result = false;
            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                Context context = Xamarin.Forms.Forms.Context;
                if (context.CheckSelfPermission(Android.Manifest.Permission.AccessFineLocation) == Permission.Granted)
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        [Obsolete]
        public bool AccessCoarseLocationPermission()
        {
            bool result = false;
            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                Context context = Xamarin.Forms.Forms.Context;
                if (context.CheckSelfPermission(Android.Manifest.Permission.AccessCoarseLocation) == Permission.Granted)
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }

        [Obsolete]
        public bool ForegroundServicePermission()
        {
            bool result = false;
            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                Context context = Xamarin.Forms.Forms.Context;
                if (context.CheckSelfPermission(Android.Manifest.Permission.ForegroundService) == Permission.Granted)
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            return result;
        }
    }
}