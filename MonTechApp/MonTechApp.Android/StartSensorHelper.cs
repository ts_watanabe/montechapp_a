﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

using Xamarin.Forms;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using MonTechApp.Interface;
using MonTechApp.Helper;
using MonTechApp.Commons;
using TCLLIB.Sensors;
using TCLLIB;
using Android.Support.V4.App;


//[assembly: Dependency(typeof(StartServiceAndroid))]
[assembly: Xamarin.Forms.Dependency(typeof(MonTechApp.Droid.StartSensorHelperAndroid))]
namespace MonTechApp.Droid
{
    public class StartSensorHelperAndroid : IStartService
    {
        static readonly string TAG = typeof(StartSensorHelperAndroid).FullName;

        //Context context;
        //Intent intent;

        public StartSensorHelperAndroid()
        {
        }

        [Obsolete]
        public void StartForegroundServiceCompat(double wgt, double lug)
        {
            //SensorHelperインテントを作成し、開始パラメータを埋め込む
            Context context = Xamarin.Forms.Forms.Context;
            Intent intent = new Intent(context, typeof(ServiceSensorHelper));
            intent.PutExtra("wgt", wgt);
            intent.PutExtra("lug", lug);

            try
            {
                ComponentName res;
                if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
                {
                    res = context.StartForegroundService(intent);
                }
                else
                {
                    res = context.StartService(intent);
                }
                System.Diagnostics.Debug.WriteLine(string.Format("ClassName = {0}, PackageName = {1}", res.ClassName, res.PackageName));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }

        [Obsolete]
        public void StopForegroundServiceCompat()
        {
            try
            {
                Context context = Xamarin.Forms.Forms.Context;
                Intent intent = new Intent(context, typeof(ServiceSensorHelper));
                context.StopService(intent);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }
    }

#if ZZZ
    public class SensorHelperBinder : Binder
    {
        public SensorHelperBinder(ServiceSensorHelper service)
        {
            this.Service = service;
        }

        public ServiceSensorHelper Service { get; private set; }
    }


    //[Service(Name = "com.Technosolutioin.App.BackgroundService", Exported = true, Process = ":Process")]
    [Service]
    public class mySensorService : Service
    {
        static readonly string TAG = typeof(mySensorService).FullName;

        public IBinder Binder { get; private set; }

        SensorHelper sensors;
        public const int SERVICE_RUNNING_NOTIFICATION_ID = 10000;

        public override void OnCreate()
        {
            base.OnCreate();
            System.Diagnostics.Debug.WriteLine(string.Format("{0} - OnCreate", TAG));
        }


        public override IBinder OnBind(Intent intent)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} - OnBind", TAG));
            //throw new NotImplementedException();
            this.Binder = new SensorHelperBinder(this);
            return this.Binder;
        }

        public override bool OnUnbind(Intent intent)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} - OnUnbind", TAG));
            return base.OnUnbind(intent);
        }

        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {
            System.Diagnostics.Debug.WriteLine(string.Format("{0} - OnStartCommand", TAG));

            //Android8対策その２（通知用チャンネル作成）
            var channelId = "com.companyname.MonTechApp";
            var channelName = "mySensorService channel";

            try
            {
                if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
                {

                }

                //SensorHelperをインスタンス化し、インテントから開始パラメータを受け取る
                sensors = new SensorHelper();
                sensors.values.psn_wgt = intent.GetDoubleExtra("wgt", 0);
                sensors.values.psn_lug = intent.GetDoubleExtra("lug", 0);
                sensors.Start();

                RegisterForegroundService();

                //var notificationApp = new Notification.Builder(this)
                //    .SetContentTitle("健康登山アプリ")
                //    .SetContentText("フォアグラウンドサービスの開始")
                //    .SetSmallIcon(Resource.Drawable.abc_dialog_material_background)
                //    //.SetContentIntent(BuildIntentToShowMainActivity())
                //    .SetOngoing(true)
                //    //.AddAction(BuildRestartTimerAction()),
                //    //.AddAction(BuildStopServiceAction())
                //    .Build();
                //System.Diagnostics.Debug.WriteLine("* mySensor Service Start");
                //StartForeground(SERVICE_RUNNING_NOTIFICATION_ID, notificationApp);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Failed service start.");
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }

            return base.OnStartCommand(intent, flags, startId);
        }

        [Obsolete]
        void RegisterForegroundService()
        {
            Context context = Xamarin.Forms.Forms.Context;
            NotificationManager manager = (NotificationManager)context.GetSystemService(Context.NotificationService);
            string channnelId = "MonTechApp Notification Channel";
            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                string channelNm = "MonTechApp";
                var importance = NotificationManager.ImportanceHigh;
                NotificationChannel channel = new NotificationChannel(channnelId, channelNm, NotificationImportance.Default);
                channel.Description = "フォアグラウンドサービスの開始";
                manager.CreateNotificationChannel(channel);
            }

            var notificationApp = new Notification.Builder(this)
                .SetContentTitle("健康登山アプリ")
                .SetContentText("フォアグラウンドサービスの開始")
                .SetSmallIcon(Resource.Drawable.abc_dialog_material_background)
                //.SetContentIntent(BuildIntentToShowMainActivity())
                .SetOngoing(true)
                //.AddAction(BuildRestartTimerAction()),
                //.AddAction(BuildStopServiceAction())
                .SetChannelId(channnelId)
                .Build();

            StartForeground(SERVICE_RUNNING_NOTIFICATION_ID, notificationApp);

        }

    }

#endif

}