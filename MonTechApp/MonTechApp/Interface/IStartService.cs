﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonTechApp.Interface
{
    public interface IStartService
    {
        void StartForegroundServiceCompat(double wgt, double lug);

        void StopForegroundServiceCompat();
    }
}
