﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonTechApp.Interface
{

    public class LocationEventArgs : EventArgs
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public double Accuracy { get; set; }

        public double Altitude { get; set; }
    }

    public delegate void LocationEventHandler(object sender, LocationEventArgs args);

    public interface IGeoLocator
    {
        void StartGps();

        event LocationEventHandler LocationReceived;
    }
}
