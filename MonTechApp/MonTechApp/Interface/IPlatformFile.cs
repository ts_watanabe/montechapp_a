﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TCLLIB.Sensors;

namespace MonTechApp.Interface
{
    public interface IPlatformFile
    {
        string GetRootFolder();

        string GetOutPutFolder();

        Task<bool> CreateFile(string filename);

        Task<bool> AppendLine(string filename, string text);

        Task<bool> AppendClimbingData(string filename, ClimbingData value);
    }
}
