﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonTechApp.Interface
{
    public interface ISleepControl
    {
        void SleepDisabled();

        void SleepEnabled();
    }
}
