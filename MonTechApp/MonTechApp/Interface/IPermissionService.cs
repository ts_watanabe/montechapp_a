﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonTechApp.Interface
{
    public interface IPermissionService
    {
        bool ExtraStragePermission();

        bool AccessFineLocationPermission();

        bool AccessCoarseLocationPermission();

        bool ForegroundServicePermission();
    }
}
