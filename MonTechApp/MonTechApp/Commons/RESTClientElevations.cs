﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TCLLIB;

namespace MonTechApp.Commons
{
    class RESTClientElevations : IDisposable
    {
        HttpClient _client;

        /// <summary>
        /// デリゲート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnElevationAccess(object sender, ElevationRequestEventArgs e);

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        public event EventHandler<ElevationRequestEventArgs> ElevationAccess;

        /// <summary>
        /// レスポンスデータ（jsonの内容）
        /// </summary>
        public Dictionary<string, string> Response;



        public RESTClientElevations()
        {
            _client = new HttpClient();

            Response = new Dictionary<string, string>();
        }

        public async void RequestElevation(double latitude, double longitude)
        {
            try
            {
                Task<Dictionary<string, string>> values = GetElevation(latitude, longitude);

                ResponseResut(values.Result);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool CheckResponce(Dictionary<string, string> response)
        {
            bool result = true;

            return result;
        }

        public async Task<Dictionary<string, string>> GetElevation(double lat, double lon)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();

            string url = @"https://cyberjapandata2.gsi.go.jp/general/dem/scripts/getelevation.php?lon={0}&lat={1}&outtype=JSON";

            string requestUri = url.Formats(lon, lat);

            var uri = new Uri(requestUri);

            try
            {
                var res = await _client.GetAsync(uri).ConfigureAwait(false);
                if (res.IsSuccessStatusCode)
                {
                    var content = await res.Content.ReadAsStringAsync();
                    values = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return values;
        }


        private void ResponseResut(Dictionary<string, string> values)
        {
            if (ElevationAccess == null)
            {
                return;
            }

            ElevationRequestEventArgs arg = new ElevationRequestEventArgs();
            if (values.ContainsKey("elevation"))
            {
                arg.status = values["elevation"].IsNumeric() ? 0 : -1;
                arg.elevation = values["elevation"].ToDouble(0d);
                arg.hsrc = values["hsrc"];
            }
            ElevationAccess(this, arg);
        }

        public void Dispose()
        {
            //
            if (_client != null)
            {
                _client.Dispose();
            }
        }
    }

    public class ElevationRequestEventArgs : EventArgs
    {
        public double elevation { get; set; }
        public string hsrc { get; set; }
        
        public int status { get; set; }

        public ElevationRequestEventArgs()
        {
        }
    }


}
