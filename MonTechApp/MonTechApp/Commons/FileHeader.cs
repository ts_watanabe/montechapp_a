﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonTechApp.Commons
{
    public class FileHeader
    {
        static public string HeaderLine()
        {
            string result = "";
            try
            {
                var header = new[]
                {
                    "mtid",
                    "climbno",
                    "rcd_tm",
                    "gpslat",
                    "gpslon",
                    "gpsalt",
                    "wgsalt",
                    "gpsacc",
                    "gpsrgb",
                    "presnow",
                    "pressea",
                    "presalt",
                    "tilex",
                    "tiley",
                    "tilezoom",
                    "dst_vrt",
                    "dst_hrt",
                    "spd_vrt",
                    "ergcsn_rt",
                    "mets_sp",
                    "ttl_mvsec",
                    "ttl_mvdst",
                    "ttl_up_mvdst",
                    "ttl_dw_mvdst",
                    "ttl_ergcsn",
                    "mets_ave",
                    "psn_wgt",
                    "psn_lug",
                    "gpslat_crt",
                    "gpslon_crt",
                    "gpsalt_crt",
                    "wgsalt_crt",
                    "presnow_crt",
                };
                
                //result = string.Join(",", header);

                //デバッグ用　//////////////////////////////////////////////////////////////
                var header_dbg = new[]
                {
                    "xxxx",
                    "xxxx",
                    "time",
                    "gps_latitude",
                    "gps_longitude",
                    "gps_altitude",
                    "gps_altitude_wgs",
                    "gps_accuracy",
                    "xxxx",
                    "xxxx",
                    "xxxx",
                    "xxxx",
                    "xxxx",
                    "xxxx",
                    "xxxx",
                    "distance_vertical",
                    "distance_horizontal",
                    "speed_vertical",
                    "energyConsumptionRate",
                    "METs_specific",
                    "otal_movingTime_sec",
                    "total_distance",
                    "total_up_distance",
                    "total_down_distance",
                    "total_energyConsumption",
                    "METs_average",
                    "person_weight",
                    "person_luggage",
                    "xxxx",
                    "xxxx",
                    "xxxx",
                    "xxxx",
                    "xxxx",
                };

                result = string.Format("{0}\n{1}", string.Join(",", header), string.Join(",", header_dbg));

                //////////////////////////////////////////////////////////////////////////
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
            return result;
        }
    }
}
