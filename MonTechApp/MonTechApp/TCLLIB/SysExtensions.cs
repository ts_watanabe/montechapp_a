﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Data;
using System.Security.Cryptography;

namespace TCLLIB
{
    /// <summary>
    /// 拡張メソッド定義クラス
    /// </summary>
    /// <remarks>
    /// 各メソッドのregionで {作用する型} 内容 を記載しています。 
    /// </remarks>
    static class SysExtensions
    {

        #region {Datatable} DataTable内の指定した列のみで構成されたDataTableのクローンを作成する。
        /// <summary>
        /// DataTable内の指定した列のみで構成されたDataTableのクローンを作成する。
        /// </summary>
        /// <param name="vInTbl"></param>
        /// <param name="columns"></param>
        /// <returns></returns>
        public static DataTable SelectColumns(this DataTable vInTbl, string[] columns)
        {
            DataTable tbl = vInTbl.Copy();
            List<string> removeColumn = new List<string>();
            foreach(DataColumn col in tbl.Columns)
            {
                if (!columns.Contains(col.ColumnName))
                {
                    removeColumn.Add(col.ColumnName);
                }
            }
            foreach(string name in removeColumn)
            {
                tbl.Columns.Remove(name);
            }
            return tbl;
        }
        #endregion

        #region {object} Objectからstringへの変換
        /// <summary>
        /// Objectからstringへの変換
        /// </summary>
        /// <param name="vInobj"></param>
        /// <returns></returns>
        /// <remarks>
        /// DBNullはstring.emptyを返します。
        /// </remarks>
        public static string ToText(this object vInobj)
        {
            string str = string.Empty;
            try
            {
                str = ((DBNull.Value.Equals(vInobj)) ? string.Empty : Convert.ToString(vInobj).Trim().Replace("\v", ""));
            }
            catch
            {
                str = string.Empty;

            }
            return str;
        }
        #endregion

        #region {object} ObjectからDateTimeへの変換
        /// <summary>
        /// Convert Object to DateTime
        /// </summary>
        /// <param name="vInobj"></param>
        /// <returns></returns>
        public static DateTime ToDatetime(this object vInobj)
        {
            DateTime dt = new DateTime(0);
            try
            {
                dt = DateTime.Parse(vInobj.ToString());
            }catch
            {
                dt = new DateTime(0);
            }
            return dt;
        }
        #endregion

        #region {object} Objectからintへの変換
        /// <summary>
        /// Convert object to int
        /// </summary>
        /// <param name="vInobj"></param>
        /// <returns></returns>
        public static int ToInt(this object vInobj)
        {
            int ret = 0;
            try
            {
                int.TryParse(ToText(vInobj), out ret);
            }
            catch
            {
                ret = 0;
            }

            return ret;

        }
        #endregion

        #region {object} Objectからintへの変換（デフォルト値指定あり）
        /// <summary>
        /// Convert object to int with default value
        /// </summary>
        /// <param name="vInobj"></param>
        /// <param name="defaultValue">変換できない場合のデフォルト値</param>
        /// <returns></returns>
        public static int ToInt(this object vInobj,int defaultValue)
        {
            int ret = defaultValue;
            try
            {
                if (!int.TryParse(ToText(vInobj), out ret))
                {
                    ret = defaultValue;
                }
            }
            catch
            {
                ret = defaultValue;
            }

            return ret;

        }
        #endregion

        #region {uint} 指定した属性を含むか検査する
        /// <summary>
        /// 指定した属性を含むか検査する
        /// </summary>
        /// <param name="vInobj"></param>
        /// <param name="attributes"></param>
        /// <returns>対象に引数attributesが含まれる場合はtrue、含まれない場合はfalseを返す</returns>
        /// <remarks>
        /// システム定数等の変数に該当の定数値が含まれるか判断する。
        /// 例）D = (A | B | C) のとき、D & B ならtrue
        /// </remarks>
        public static bool HaveAttributes(this uint vInobj, uint attributes)
        {
            bool ret = false;
            try
            {
                if ((vInobj & attributes) == 0)
                {
                    ret = false;
                }else
                {
                    ret = true;
                }
            }catch(Exception)
            {
                ret = false;
            }
            return ret;
        }
        #endregion

        #region {object} ObjectからDoubleへの変換
        /// <summary>
        /// Convert Object to Double
        /// </summary>
        /// <param name="vInobj"></param>
        /// <returns></returns>
        public static double ToDouble(this object vInobj)
        {
            return vInobj.ToDouble(0d);
        }
        #endregion

        public static double ToDouble(this object vInobj, double defaultValue)
        {
            double ret = 0;
            try
            {
                string value = ToText(vInobj);
                if (value.IndexOf('%') < 0)
                {
                    //％表記でなければそのまま変換
                    double.TryParse(ToText(vInobj), out ret);
                }
                else
                {
                    //％表記の場合は、100で割った数値を返す
                    value = value.Replace("%", "");
                    double.TryParse(value, out ret);
                    ret = ret / 100d;
                }
            }
            catch
            {
                ret = defaultValue;
            }
            return ret;
        }


        #region {string} string.IsNullOrEmpty()の拡張
        /// <summary>
        /// string.IsNullOrEmpty()の拡張
        /// </summary>
        /// <param name="vInstr"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string vInstr)
        {
            return String.IsNullOrEmpty(vInstr);
        }
        #endregion

        #region {string} 指定した文字列がNullOrEmptyではなければTrue
        public static bool IsNotNullOrEmpty(this string vInstr)
        {
            return !String.IsNullOrEmpty(vInstr);
        }
        #endregion

        #region {string} stringへのIsNumeric実装
        public static bool IsNumeric(this string vInstr)
        {
            bool result = false;
            try
            {
                //doubleに変換できるか確かめる
                double d;
                if (double.TryParse(vInstr, out d))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
        #endregion

        #region {string} string.Format()の拡張
        /// <summary>
        /// string.Format()の拡張
        /// </summary>
        /// <param name="format"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static string Formats(this string format, params object[] values)
        {
            return String.Format(format, values);
        }
        #endregion

        #region {string} 指定した文字列を指定した位置で分割し配列に格納する
        /// <summary>
        /// 指定した文字列を指定した位置で分割し配列に格納する。
        /// srcの文字長さがvaluesで指定した分割長さ未満の場合は例外が発生する。
        /// </summary>
        /// <param name="src">分割対象の文字列</param>
        /// <param name="values">分割文字数</param>
        /// <returns>分割された文字列の配列</returns>
        /// <example>
        /// ex1) 郵便番号を上３桁下４桁に分割する
        /// <code>
        /// string[] zip = "1040033".Divide(3, 4);
        /// </code>
        ///  -> zip[0] = "104"
        ///     zip[1] = "0033"
        /// 
        /// ex2) 年月日を年/月/日に分割する
        /// <code>
        /// string[] ymd = "20191020".Divide(4, 2, 2);
        /// </code>
        /// 
        ///  -> ymd[0] = "2019"
        ///     ymd[1] = "10"
        ///     ymd[2] = "20"
        /// </example>
        public static string[] Divide(this string src, params int[] values)
        {
            int l = values.Sum();
            if (src.Length != l)
            {
                throw new Exception("分割対象の文字数が入力文字の長さを超えています。");
            }
            string[] result = new string[values.Length];
            int p = 0;
            for(int i = 0; i < values.Length; i++)
            {
                result[i] = src.Substring(p, values[i]);
                p += values[i];
            }
            return result;
        }
        #endregion

        #region {double} System.Math.Round()の拡張
        /// <summary>
        /// System.Math.Round()の拡張(1/2)
        /// </summary>
        /// <param name="vInval"></param>
        /// <param name="roundpoint">丸める桁数</param>
        /// <returns></returns>
        public static double Round(this double vInval,int roundpoint)
        {
            double ret = 0;
            try
            {
                ret = Math.Round(vInval, roundpoint);
            }catch
            {
                ret = 0;
            }
            return ret;
        }

        /// <summary>
        /// System.Math.Round()の拡張(2/2)
        /// </summary>
        /// <param name="vInval"></param>
        /// <param name="roundpoint">丸める桁数</param>
        /// <param name="mode">モード</param>
        /// <returns></returns>
        public static double Round(this double vInval, int roundpoint, MidpointRounding mode)
        {
            double ret = 0;
            try
            {
                ret = Math.Round(vInval, roundpoint, mode);
            }
            catch
            {
                ret = 0;
            }
            return ret;
        }
        #endregion

        #region {T} 指定された値が、指定した範囲（start ～ end）内にあるか判定する
        /// <summary>
        /// Comparerに対応した型が範囲内にあるか判定する
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static bool IsBetween<T>(this T item, T start, T end)
        {
            return Comparer<T>.Default.Compare(item, start) >= 0
                && Comparer<T>.Default.Compare(item, end) <= 0;
        }
        #endregion

        #region {object} 空値（Empty）であるか判定する
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vInObj"></param>
        /// <returns></returns>
        public static bool IsEmpty(this object vInObj)
        {
            bool result = false;
            if (vInObj == null)
            {
                result = true;
            }
            else
            {
                switch (vInObj.GetType().Name)
                {
                    case "String":
                        result = vInObj.ToString().Equals(string.Empty);
                        break;
                    case "DBNull":
                        //DBNullは無条件にEmpty
                        result = true;
                        break;
                    default:
                        result = false;
                        break;
                }
            }
            return result;
        }
        #endregion

        #region {string} 指定した文字列が指定した文字列リスト内に存在するか判定する
        /// <summary>
        /// 集合判定の簡素化関数
        /// </summary>
        /// <param name="target"></param>
        /// <param name="arg"></param>
        /// <returns></returns>
        /// <example>
        /// <code>
        /// string devicename = "usb";
        /// bool result = devicename.InArray("con", "kybd", "prn");
        /// </code>
        /// -> result の値は false
        /// </example>
        public static bool InArray(this string target, params string[] arg)
        {
            bool result = false;
            try
            {
                ArrayList ary = new ArrayList(arg);
                result = (ary.IndexOf(target) > -1);
            }
            catch (Exception ex)
            {
                throw new Exception("集合判定でのエラー target=" + target + " args:" + string.Join(",", arg), ex);
            }
            return result;
        }
        #endregion

        #region {IDictionary<TKey, TValue>} Dictionaryに指定したDictionaryを丸ごと追加
        /// <summary>
        /// Dictionaryに指定したDictionaryを丸ごと追加
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="source"></param>
        /// <param name="addPairs"></param>
        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> source, IEnumerable<KeyValuePair<TKey, TValue>> addPairs)
        {
            //同一キーは上書き
            source.AddRange(addPairs, true);
        }
        /// <summary>
        /// Dictionaryに指定したDictionaryを丸ごと追加
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="source">追加先</param>
        /// <param name="addPairs">追加するディクショナリ</param>
        /// <param name="overwrite">true:同一キーは上書き、false:同一キーはパス</param>
        public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> source, IEnumerable<KeyValuePair<TKey, TValue>> addPairs, bool overwrite)
        {
            foreach (var kv in addPairs)
            {
                if (source.ContainsKey(kv.Key))
                {
                    if (overwrite)
                    {
                        source[kv.Key] = kv.Value;
                    }
                }else
                {
                    source.Add(kv);
                }
            }
        }
        #endregion

        #region {IDictionary<TKey, TValue>} Dictionaryのダンプ文字列を作成する
        /// <summary>
        /// Dictionaryのダンプ文字列を作成する
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        /// <remarks>
        /// 主にデバッグ用
        /// 入れ子になったDictionaryやListは型のみ表示される。
        /// </remarks>
        public static string ToDumpString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            return "{" + string.Join(",", dictionary.Select(kv => kv.Key + "=" + kv.Value).ToArray()) + "}";
        }
        #endregion


        #region {string} 指定した文字列の先頭（左）から指定した長さの文字列を抽出する
        public static string Left(this string vInstr, int len)
        {
            string result = "";
            try
            {
                if ((vInstr.Length == 0) || (vInstr.Length < len))
                {
                    throw new ArgumentException("文字列 {0} は、{1}文字より以上でなければなりません.".Formats(vInstr, len));
                }
                result = vInstr.Substring(0, len);
            }catch(Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }
        #endregion

        #region {string} 指定した文字列の後端（右）から指定した長さの文字列を抽出する
        public static string Right(this string vInstr, int len)
        {
            string result = "";
            try
            {
                if ((vInstr.Length == 0) || (vInstr.Length < len))
                {
                    new ArgumentException("文字列 {0} は、{1}文字より以上でなければなりません.".Formats(vInstr, len));
                }
                result = vInstr.Substring(vInstr.Length - len, len);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;

        }
        #endregion
    }

}
