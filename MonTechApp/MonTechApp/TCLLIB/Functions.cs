﻿using System;
using System.Collections.Generic;
using System.Text;
using MonTechApp.Exceptions;

namespace TCLLIB
{
    class Functions
    {
        public static (double picx, double picy) ConvertGpsToPixel(double lat, double lon, int zoom)
        {
            (double x, double y) result = (0, 0);
            double pi = Math.PI;
            double L = 85.05112878;
            try
            {
                double x = Math.Floor((Math.Pow(2, (zoom + 7))) * ((lon / 180) + 1));
                double y = Math.Floor((((Math.Pow(2, (zoom + 7))) / pi) * ((-1 * Atanh(Math.Sin((pi / 180) * lat))) + (Atanh(Math.Sin((pi / 180) * L))))));

                result = (x, y);
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;

        }

        #region [Method] GPSからの座標を基にタイル番号を取得する
        /// <summary>
        /// 緯度、経度、表示倍率をもとにタイル番号を求める
        /// </summary>
        /// <param name="lat">緯度</param>
        /// <param name="lon">経度</param>
        /// <param name="zoom">ズームレベル</param>
        /// <returns>
        ///     
        /// </returns>
        public static (int tilex, int tiley) GetTileIndex(double lat, double lon, int zoom)
        {
            (int x, int y) result = (0, 0);
            try
            {
                (double x, double y) = ConvertGpsToPixel(lat, lon, zoom);
                int tx = (int)Math.Floor(x / (double)256);
                int ty = (int)Math.Floor(y / (double)256);
                result = (tx, ty);
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        #endregion

        /// <summary>
        /// 緯度、経度からタイル内座標を取得する
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <param name="zoom"></param>
        /// <returns></returns>
        public static (double posx, double posy) GetPositionInTile(double lat, double lon, int zoom)
        {
            (int x, int y) result = (0, 0);
            try
            {
                (double x, double y) = ConvertGpsToPixel(lat, lon, zoom);
                int tx = (int)Math.Floor(x % (double)256);
                int ty = (int)Math.Floor(y % (double)256);
                result = (tx, ty);
            }
            catch (Exception e)
            {
                throw e;
            }
            return result;

        }

        #region [Method] 引数の双曲線逆正接（ArcTanH）を求める
        /// <summary>
        /// 引数の双曲線逆正接（ArcTanH）を求める
        /// </summary>
        /// <param name="x"></param>
        /// <returns>
        ///     double
        /// </returns>
        private static double Atanh(double x)
        {
            return (Math.Log((1 + x) / (1 - x)) / 2);
        }
        #endregion

        public double ComputeElevator(double seapressure, double pressure, double tempalature)
        {
            double result = 0;
            try
            {
                result = ((Math.Pow((seapressure / pressure), (1 / 5.257)) - 1) * (tempalature + 273.15)) / 0.0065;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                result = 0;
            }
            return result;
        }

        public static double ComputeElevator(int red, int green, int blue)
        {
            double mAltitude = 0.0;

            double RESOLUTION = 0.01;           //標高分解能
            double h = (double)red * Math.Pow(2, 16) + (double)green * Math.Pow(2, 8) + (double)blue;   //閾値

            if (h <= Math.Pow(2, 23))
            {
                mAltitude = h * RESOLUTION;
            }
            else if (h > Math.Pow(2, 23))
            {
                mAltitude = (h - Math.Pow(2, 24)) * RESOLUTION;
            }

            return mAltitude;
        }

        #region [Method] 現在地の標高、気温、気圧から海面気圧を求める
        /// <summary>
        /// 現在地の標高、気温、気圧から海面気圧を求める
        /// </summary>
        /// <param name="elevation"></param>
        /// <param name="temperature"></param>
        /// <param name="pressure"></param>
        /// <returns></returns>
        public static double ComputeSeaPuressure(double elevation, double temperature, double pressure)
        {
            double result = 0;
            try
            {
                result = pressure * Math.Pow((1 - ((0.0065 * elevation) / (temperature + (0.0065 * elevation) + 273.15))), -5.257);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
            return result;
        }
        #endregion

    }
}
