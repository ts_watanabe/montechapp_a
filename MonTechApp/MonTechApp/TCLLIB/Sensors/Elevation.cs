﻿using System;
using System.Collections.Generic;
using System.Text;
using MonTechApp.Exceptions;
using System.Resources;
using Xamarin.Forms.Internals;
using MonTechApp.Commons;

namespace TCLLIB.Sensors
{
    /// <summary>
    /// 標高クラス
    /// </summary>
    /// <remarks>
    /// Description : 標高判定を一元管理するクラス
    /// Create : 2020/04/23 テクノ渡辺
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class Elevation
    {
        public const double SEA_PRESSURE_DEFAULT = 1013.25;

        #region プロパティ

        /// <summary>
        /// 現在地気圧
        /// </summary>
        double Pressure {
            get 
            {
                return _lastPressure;
            }
            set
            {
                _lastPressure = value;
            } 
        }

        /// <summary>
        /// 海面気圧
        /// </summary>
        public double SeaPressure { get; set; }

        /// <summary>
        /// 気温
        /// </summary>
        public double Temperature { get; set; }

        /// <summary>
        /// 標高
        /// </summary>
        /// <remarks>
        /// ReadOnly
        /// </remarks>
        public double Elevator
        {
            get
            {
                return _lastElevator;
            }
        }

        #endregion

        #region イベントハンドラ

        /// <summary>
        /// デリゲート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnElevatorChanged(object sender, ElevetorChangedEventArgs e);

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        public event EventHandler<ElevetorChangedEventArgs> ElevatorChanged;

        #endregion

        #region クラス変数

        private BarometerWrapper _barometer;

        /// <summary>
        /// 最終確認気圧 [hPa]
        /// </summary>
        private double _lastPressure;

        /// <summary>
        /// 最終確認標高 [m]
        /// </summary>
        private double _lastElevator;

        #endregion

        public Elevation()
        {

            _barometer = new BarometerWrapper();

            _barometer.ReadingChanged += this.BarometerChanged;

            Pressure = 0;
            Temperature = 0;
            SeaPressure = Elevation.SEA_PRESSURE_DEFAULT;

            _lastElevator = 0;
        }

        void BarometerChanged(object sender, BarometerReadingChangedEventArgs e)
        {
            try
            {
                _lastPressure = e.PressureInHectopascals;

                //必要な値のチェック
                bool errflg = this.CheckParameters();
                if (this.CheckParameters())
                {
                    double elev = ComputeElevator(this.SeaPressure, _lastPressure, Temperature);
                    if (elev != _lastElevator)
                    {
                        _lastElevator = elev;
                        if (ElevatorChanged != null)
                        {
                            ElevetorChangedEventArgs arg = new ElevetorChangedEventArgs();
                            arg.Elevator = _lastElevator;
                            arg.PressureInHectopascals = _lastPressure;
                            ElevatorChanged(this, arg);
                        }
                    }
                }else{
                    //[TODO]実装元の状態更新イベントを呼ぶようにする
                    //throw new INVALID_INPUT_DATA(AppResources.ERRMSG_001);
                }
            }
            catch(INVALID_INPUT_DATA ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                //logger.write(ex.Message());
                string errmsg = string.Format("入力値:SeaPressure={0}, LastPressure={1}", this.SeaPressure, _lastPressure);
                throw new Exception(string.Format("標高取得で例外発生 {0}", errmsg), ex);
            }
        }

        public void Start()
        {
            try
            {
                _barometer.Start();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }

        public void Stop()
        {
            try
            {
                _barometer.Stop();
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }

        private bool CheckParameters()
        {
            bool result = true;
            try
            {
                if (Temperature == 0)
                {
                    result = false;
                }
                if (_lastPressure == 0)
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public double NowElevator()
        {
            double result = 0;
            try
            {
                if (this.CheckParameters())
                {
                    result = ComputeElevator(this.SeaPressure, _lastPressure, Temperature);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
            return result;
        }

        private double ComputeElevator(double seapressure, double pressure, double tempalature)
        {
            double result = 0;
            try
            {
                result = ((Math.Pow((seapressure / pressure), (1 / 5.257)) - 1) * (tempalature + 273.15)) / 0.0065;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                result = 0;
            }
            return result;
        }

        #region [Method] 現在地の標高、気温から海面気圧を求める
        /// <summary>
        /// 現在地の標高、気温から海面気圧を求める
        /// </summary>
        /// <param name="elevation"></param>
        /// <param name="temperature"></param>
        /// <param name="pressure"></param>
        /// <returns></returns>
        public double ComputeSeaPuressure(double elevation, double temperature)
        {
            double result = 0;
            try
            {
                result = _lastPressure * Math.Pow((1 - ((0.0065 * elevation) / (temperature + (0.0065 * elevation) + 273.15))), -5.257);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
            return result;
        }
        #endregion

        #region [Method] 現在地の標高、気温、気圧から海面気圧を求める
        /// <summary>
        /// 現在地の標高、気温、気圧から海面気圧を求める
        /// </summary>
        /// <param name="elevation"></param>
        /// <param name="temperature"></param>
        /// <param name="pressure"></param>
        /// <returns></returns>
        public double ComputeSeaPuressure(double elevation, double temperature, double pressure)
        {
            double result = 0;
            try
            {
                result = pressure * Math.Pow((1 - ((0.0065 * elevation) / (temperature + (0.0065 * elevation) + 273.15))), -5.257);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
            return result;
        }
        #endregion


    }

    /// <summary>
    /// 標高値取得イベント用引数クラス
    /// </summary>
    /// <remarks>
    /// Description : イベントで取得した値またはエラー情報を格納するクラス
    /// Create : 2020/04/23 テクノ渡辺
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class ElevetorChangedEventArgs : EventArgs
    {
        public double PressureInHectopascals { get; set; }
        public double Elevator { get; set; }
        public ElevetorChangedEventArgs()
        {
        }
    }

}
