﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace TCLLIB.Sensors
{
    class GyroscopeWrapper : SensorBase
    {
        #region クラス変数
        #endregion

        #region プロパティ
        public bool IsMonitoring
        {
            get
            {
                bool result = false;
                try
                {
                    result = Gyroscope.IsMonitoring;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return result;
            }
        }
        #endregion

        #region イベントハンドラ
        /// <summary>
        /// デリゲート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnReadingChanged(object sender, GyroscopeReadingChangedEventArgs e);

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        public event EventHandler<GyroscopeReadingChangedEventArgs> ReadingChanged;
        #endregion


        #region コンストラクタ
        public GyroscopeWrapper()
        {
            //イベントアサイン
            Gyroscope.ReadingChanged += Gyroscope_ReadingChanged;
        }
        #endregion

        #region メソッド

        #region Start
        /// <summary>
        /// Start
        /// </summary>
        /// <param></param>
        public bool Start()
        {
            bool result = false;
            try
            {
                result = Start(base.speed);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Start
        /// <summary>
        /// Start
        /// </summary>
        /// <param name="speed"></param>
        public bool Start(SensorSpeed speed)
        {
            bool result = false;
            try
            {
                if (!Gyroscope.IsMonitoring)
                {
                    Gyroscope.Start(speed);
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Stop
        /// <summary>
        /// Stop
        /// </summary>
        /// <param></param>
        public bool Stop()
        {
            bool result = false;
            try
            {
                if (Gyroscope.IsMonitoring)
                {
                    Gyroscope.Stop();
                }
                result = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                //派生元をdispose
                base.Dispose(disposing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region [Event] ジャイロスコープの値変化イベント
        /// <summary>
        ///     ジャイロスコープの値変化イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Gyroscope_ReadingChanged(object sender, GyroscopeChangedEventArgs e)
        {
            var data = e.Reading;
            if (ReadingChanged != null)
            {
                GyroscopeReadingChangedEventArgs args = new GyroscopeReadingChangedEventArgs(data.AngularVelocity.X, data.AngularVelocity.Y, data.AngularVelocity.Z);
                ReadingChanged(this, args);
            }
        }
        #endregion

        #region [Method][async] 
        #endregion

        #endregion
    }


    #region イベントクラス
    /// <summary>
    ///     ジャイロセンサのイベントクラス
    /// </summary>
    /// <remarks>
    /// Description :
    /// Create : 2020/04/27 テクノ古橋
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class GyroscopeReadingChangedEventArgs : EventArgs
    {
        public double AngularVelocity_X { get; set; }
        public double AngularVelocity_Y { get; set; }
        public double AngularVelocity_Z { get; set; }

        public GyroscopeReadingChangedEventArgs()
        {
        }
        public GyroscopeReadingChangedEventArgs(double x, double y, double z)
        {
            this.AngularVelocity_X = x;
            this.AngularVelocity_Y = y;
            this.AngularVelocity_Z = z;
        }

    }
    #endregion
}
