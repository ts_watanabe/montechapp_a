﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace TCLLIB.Sensors
{
    /// <summary>
    /// 登高データクラス
    /// </summary>
    public class ClimbingData
    {

        #region クラス変数
        /// <summary>
        /// 山ID
        /// </summary>
        public string mtid;
        /// <summary>
        /// 山行No
        /// </summary>
        public string climbno;
        /// <summary>
        /// 記録採集日時（yyyy-mm-dd hh:mm:ss）
        /// </summary>
        public string rcd_tm;
        
        /// <summary>
        /// GPS緯度
        /// </summary>
        public double gpslat;
        /// <summary>
        /// GPS経度
        /// </summary>
        public double gpslon;
        /// <summary>
        /// GPS標高（地理院タイル）
        /// </summary>
        public double gpsalt;
        /// <summary>
        /// WGS標高
        /// </summary>
        public double wgsalt;
        /// <summary>
        /// GPS正確度（Accuracy）
        /// </summary>
        public double gpsacc;
        /// <summary>
        /// GPS標高RGB値
        /// </summary>
        public string gpsrgb;
        
        /// <summary>
        /// 現在地気圧
        /// </summary>
        public double presnow;
        /// <summary>
        /// 海面気圧
        /// </summary>
        public double pressea;
        /// <summary>
        /// 気圧による標高
        /// </summary>
        public double presalt;

        /// <summary>
        /// GPS標高タイルNo(X)
        /// </summary>
        public int tilex;
        /// <summary>
        /// GPS標高タイルNo(Y)
        /// </summary>
        public int tiley;
        /// <summary>
        /// GPS標高タイルZoom値
        /// </summary>
        public int tilezoom;

        /// <summary>
        /// 垂直方法の移動距離(m)
        /// </summary>
        public double dst_vrt;
        /// <summary>
        /// 水平方向の移動距離(m)
        /// </summary>
        public double dst_hrt;
        /// <summary>
        /// 垂直方向の移動速度
        /// </summary>
        public double spd_vrt;
        /// <summary>
        /// エネルギー消費量率（kcal/min)
        /// </summary>
        public double ergcsn_rt;
        /// <summary>
        /// 運動強度（瞬間値）（METs)
        /// </summary>
        public double mets_sp;

        /// <summary>
        /// 累積移動時間(sec)
        /// </summary>
        public double ttl_mvsec;
        /// <summary>
        /// 水平方向の累積移動距離(m)
        /// </summary>
        public double ttl_mvdst;
        /// <summary>
        /// 垂直方向（登り）の累積移動距離(m)
        /// </summary>
        public double ttl_up_mvdst;
        /// <summary>
        /// 垂直方向（下り）の累積移動距離(m)
        /// </summary>
        public double ttl_dw_mvdst;

        /// <summary>
        /// 累積エネルギー消費量(kcal)
        /// </summary>
        public double ttl_ergcsn;
        /// <summary>
        /// 平均の運動強度(METs)
        /// </summary>
        public double mets_ave;

        /// <summary>
        /// 登山者情報：体重(Kg)
        /// 
        /// </summary>
        public double psn_wgt;
        /// <summary>
        /// 登山者情報：荷物重量(Kg)
        /// </summary>
        public double psn_lug;

        /// <summary>
        /// GPS緯度（補正値）
        /// </summary>
        public double gpslat_crt;
        /// <summary>
        /// GPS経度（補正値）
        /// </summary>
        public double gpslon_crt;
        /// <summary>
        /// GPS標高（補正値）
        /// </summary>
        public double gpsalt_crt;
        /// <summary>
        /// GPS(WGS)標高（補正値）
        /// </summary>
        public double wgsalt_crt;
        /// <summary>
        /// 気圧（補正値）
        /// </summary>
        public double presnow_crt;

        //[TODO]今後実装予定
        //ジャイロセンサの値(X)
        //ジャイロセンサの値(Y)
        //ジャイロセンサの値(Z)
        //磁気センサの値(X)
        //磁気センサの値(Y)
        //磁気センサの値(Z)
        //シェイク検出の回数（歩数計用


        #endregion


        #region コンストラクタ
        public ClimbingData()
        {
            this.mtid = "";
            this.climbno = "";
            this.rcd_tm = "";
            this.gpslat = 0.0;
            this.gpslon = 0.0;
            this.gpsalt = 0.0;
            this.wgsalt = 0.0;
            this.gpsacc = 0.0;
            this.gpsrgb = "";
            this.presnow = 0.0;
            this.pressea = 0.0;
            this.presalt = 0.0;
            this.tilex = 0;
            this.tiley = 0;
            this.tilezoom = 0;
            //瞬間値
            this.dst_vrt = 0.0;
            this.dst_hrt = 0.0;
            this.spd_vrt = 0.0;
            this.ergcsn_rt = 0.0;
            this.mets_sp = 0.0;
            //累積値
            this.ttl_mvsec = 0.0;
            this.ttl_mvdst = 0.0;
            this.ttl_up_mvdst = 0.0;
            this.ttl_dw_mvdst = 0.0;
            this.ttl_ergcsn = 0.0;
            this.mets_ave = 0.0;
            //入力値
            this.psn_wgt = 0.0;
            this.psn_lug = 0.0;
            //補正値
            this.gpslat_crt = 0.0;
            this.gpslon_crt = 0.0;
            this.gpsalt_crt = 0.0;
            this.wgsalt_crt = 0.0;
            this.presnow_crt = 0.0;
        }
        #endregion

        #region コンストラクタ（初期化あり）
        /// <summary>
        /// コンストラクタ（初期化あり）
        /// </summary>
        /// <param name="src"></param>
        public ClimbingData(ClimbingData src)
        {
            Clone(src);
        }
        #endregion

        /// <summary>
        /// 引数の値で自身を初期化する
        /// </summary>
        /// <param name="src"></param>
        public void Clone(ClimbingData src)
        {
            this.mtid = src.mtid;
            this.climbno = src.climbno;
            this.rcd_tm = src.rcd_tm;
            this.gpslat = src.gpslat;
            this.gpslon = src.gpslon;
            this.gpsalt = src.gpsalt;
            this.wgsalt = src.wgsalt;
            this.gpsacc = src.gpsacc;
            this.gpsrgb = src.gpsrgb;
            this.presnow = src.presnow;
            this.pressea = src.pressea;
            this.presalt = src.presalt;
            this.tilex = src.tilex;
            this.tiley = src.tiley;
            this.tilezoom = src.tilezoom;
            //瞬間値
            this.dst_vrt = src.dst_vrt;
            this.dst_hrt = src.dst_hrt;
            this.spd_vrt = src.spd_vrt;
            this.ergcsn_rt = src.ergcsn_rt;
            this.mets_sp = src.mets_sp;
            //累積値
            this.ttl_mvsec = src.ttl_mvsec;
            this.ttl_mvdst = src.ttl_mvdst;
            this.ttl_up_mvdst = src.ttl_up_mvdst;
            this.ttl_dw_mvdst = src.ttl_dw_mvdst;
            this.ttl_ergcsn = src.ttl_ergcsn;
            this.mets_ave = src.mets_ave;
            //入力値
            this.psn_wgt = src.psn_wgt;
            this.psn_lug = src.psn_lug;
            //補正値
            this.gpslat_crt = src.gpsalt_crt;
            this.gpslon_crt = src.gpslon_crt;
            this.gpsalt_crt = src.gpsalt_crt;
            this.wgsalt_crt = src.wgsalt_crt;
            this.presnow_crt = src.presnow_crt;
        }


    }
}
