﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace TCLLIB.Sensors
{
    class DetectShakeWrapper : SensorBase
    {

        #region クラス変数
        private int _shakeEventCount = 0;
        private DetectShakeWrapper _detectShake;
        #endregion

        #region プロパティ
        public bool IsMonitoring
        {
            get
            {
                bool result = false;
                try
                {
                    result = Accelerometer.IsMonitoring;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return result;
            }
        }
        #endregion

        #region イベントハンドラ
        /// <summary>
        /// デリゲート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnShakeDetected(object sender, AccelerometerShakeDetectedEventArgs e);

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        public event EventHandler<AccelerometerShakeDetectedEventArgs> ShakeDetected;
        //public event EventHandler ShakeDetected;
        #endregion


        #region コンストラクタ
        public DetectShakeWrapper()
        {
            //イベントアサイン
            //_detectShake = new DetectShakeWrapper();
            Accelerometer.ShakeDetected += Accelerometer_ShakeDetected;
        }
        #endregion

        #region メソッド

        #region Start
        /// <summary>
        /// Start
        /// </summary>
        /// <param></param>
        public bool Start()
        {
            bool result = false;
            try
            {
                result = Start(base.speed);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Start
        /// <summary>
        /// Start
        /// </summary>
        /// <param name="speed"></param>
        public bool Start(SensorSpeed speed)
        {
            bool result = false;
            try
            {
                if (!Accelerometer.IsMonitoring)
                {
                    Accelerometer.Start(speed);
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Stop
        /// <summary>
        /// Stop
        /// </summary>
        /// <param></param>
        public bool Stop()
        {
            bool result = false;
            try
            {
                if (Accelerometer.IsMonitoring)
                {
                    Accelerometer.Stop();
                }
                result = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                //派生元をdispose
                base.Dispose(disposing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region [Event] 磁気センサの値変化イベント
        /// <summary>
        ///     磁気センサの値変化イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Accelerometer_ShakeDetected(object sender, EventArgs e)
        {
            Console.WriteLine(string.Format("Accelerometer_ShakeDetected: {0}", this._shakeEventCount));
            if (ShakeDetected != null)
            {
                this._shakeEventCount += 1;
                AccelerometerShakeDetectedEventArgs args = new AccelerometerShakeDetectedEventArgs();
                args.ShakeEventCount = this._shakeEventCount;

                ShakeDetected(this, args);
            }
        }
        #endregion

        #region [Method][async] 
        #endregion


        #endregion

    }

    #region イベントクラス
    /// <summary>
    ///     加速度センサのシェイクイベントクラス
    /// </summary>
    /// <remarks>
    /// Description :
    /// Create : 2020/04/27 テクノ古橋
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class AccelerometerShakeDetectedEventArgs : EventArgs
    {
        public int ShakeEventCount { get; set; }

        public AccelerometerShakeDetectedEventArgs()
        {
        }

    }
    #endregion

}
