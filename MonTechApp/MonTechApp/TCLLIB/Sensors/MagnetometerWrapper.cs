﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace TCLLIB.Sensors
{
    class MagnetometerWrapper : SensorBase
    {
        #region クラス変数
        #endregion

        #region プロパティ
        public bool IsMonitoring
        {
            get
            {
                bool result = false;
                try
                {
                    result = Magnetometer.IsMonitoring;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return result;
            }
        }
        #endregion

        #region イベントハンドラ
        /// <summary>
        /// デリゲート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnReadingChanged(object sender, MagnetometerReadingChangedEventArgs e);

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        public event EventHandler<MagnetometerReadingChangedEventArgs> ReadingChanged;
        #endregion


        #region コンストラクタ
        public MagnetometerWrapper()
        {
            //イベントアサイン
            Magnetometer.ReadingChanged += Magnetometer_ReadingChanged;
        }
        #endregion

        #region メソッド

        #region Start
        /// <summary>
        /// Start
        /// </summary>
        /// <param></param>
        public bool Start()
        {
            bool result = false;
            try
            {
                result = Start(base.speed);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Start
        /// <summary>
        /// Start
        /// </summary>
        /// <param name="speed"></param>
        public bool Start(SensorSpeed speed)
        {
            bool result = false;
            try
            {
                if (!Magnetometer.IsMonitoring)
                {
                    Magnetometer.Start(speed);
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Stop
        /// <summary>
        /// Stop
        /// </summary>
        /// <param></param>
        public bool Stop()
        {
            bool result = false;
            try
            {
                if (Magnetometer.IsMonitoring)
                {
                    Magnetometer.Stop();
                }
                result = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                //派生元をdispose
                base.Dispose(disposing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region [Event] 磁気センサの値変化イベント
        /// <summary>
        ///     ジャイロスコープの値変化イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Magnetometer_ReadingChanged(object sender, MagnetometerChangedEventArgs e)
        {
            var data = e.Reading;
            if (ReadingChanged != null)
            {
                MagnetometerReadingChangedEventArgs args = new MagnetometerReadingChangedEventArgs(data.MagneticField.X, data.MagneticField.Y, data.MagneticField.Z);
                ReadingChanged(this, args);
            }
        }
        #endregion

        #region [Method][async] 
        #endregion


        #endregion

    }

    #region イベントクラス
    /// <summary>
    ///     磁気センサのイベントクラス
    /// </summary>
    /// <remarks>
    /// Description :
    /// Create : 2020/04/27 テクノ古橋
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class MagnetometerReadingChangedEventArgs : EventArgs
    {
        public double MagneticField_X { get; set; }
        public double MagneticField_Y { get; set; }
        public double MagneticField_Z { get; set; }

        public MagnetometerReadingChangedEventArgs()
        {
        }
        public MagnetometerReadingChangedEventArgs(double x, double y, double z)
        {
            this.MagneticField_X = x;
            this.MagneticField_Y = y;
            this.MagneticField_Z = z;
        }

    }
    #endregion

}
