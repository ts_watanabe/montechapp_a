﻿using System;
using System.Collections.Concurrent;

namespace TCLLIB.Sensors
{
    /// <summary>
    ///     移動状態の判定クラス
    /// </summary>
    /// <remarks>
    /// Description : 登山中の移動判定（上昇・下降・停止、移動距離等）をする　★TODO:Elevationとマージする必要あり
    /// Create : 2020/04/23 テクノ古橋
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class MovingWatcher
    {
        #region クラス変数
        private double _lastLatitude = 0.0;
        private double _lastLongitude = 0.0;
        private double _lastAltitude = 0.0;

        // 瞬間的な値
        private double _distance_vertical = 0.0;
        private double _distance_horizontal = 0.0;
        private double _speed_vertical = 0.0;
        private double _energyConsumptionRate = 0.0;
        private double _METs_specific = 0.0;

        // 累積的な値
        private double _total_movingTime_sec = 0.0;
        private double _total_movingDistance_meter = 0.0;
        private double _total_upMovingDistance_meter = 0.0;
        private double _total_downMovingDistance_meter = 0.0;
        private double _total_energyConsumption = 0.0;
        private double _METs_average = 0.0;

        /// <summary>
        /// 登高データキュー
        /// </summary>
        private ConcurrentQueue<ClimbingData> _queueClimbing;

        private ClimbingAbilityCalculator _cac;
        #endregion

        #region プロパティ
        public double person_weight { get; set; }
        public double person_luggage { get; set; }
        #endregion

        #region イベントハンドラ
        /// <summary>
        /// デリゲート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnMoving(object sender, MovingWatcherEventArgs e);

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        public event EventHandler<MovingWatcherEventArgs> Moving;
        #endregion


        #region コンストラクタ
        //テスト用（デフォルト値あり）
        public MovingWatcher()
        {
            this.person_weight = 60.0;
            this.person_luggage = 10.0;
            //this._cac = new ClimbingAbilityCalculator(60.0, 10.0);
            this._cac = new ClimbingAbilityCalculator();

            this._queueClimbing = new ConcurrentQueue<ClimbingData>();
        }
        //public MovingWatcher(double person_weight, double person_luggage)
        //{
        //    this._cac = new ClimbingAbilityCalculator(person_weight, person_luggage);
            
        //    this._queueClimbing = new ConcurrentQueue<ClimbingData>();
        //}
        #endregion


        #region メソッド


        //以下未実装----------------------------------------------------------------------------------------------------------------

        // 登山中、休憩中の判定
        public static int judgeClimbingSeen(int data)
        {
            int seen = 0;
            try
            {
                //フィルタ処理等を組み合わせて判定する　要検討
            }
            catch (Exception ex)
            {
                //例外発生時の処理
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
            return seen;
        }


        // 登山の状態（のぼり坂、残あり、螺旋状等）　TODO:要検証
        public static int judgeClimbingCondition(int data)
        {
            int status = 0;
            try
            {
                //フィルタ処理等を組み合わせて判定する　要検討
            }
            catch (Exception ex)
            {
                //例外発生時の処理
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
            return status;
        }


        //---------------------------------------------------------------------------------------------------------------------------



        #region [Method][async] GPSからの座標を取得し更新する
        /// <summary>
        /// GetGeoLocation()
        /// </summary>
        /// <returns>
        ///   void
        /// </returns>
        /// <remarks>
        /// Description : GPSの値を取得し、画面内に表示する非同期メソッド
        /// Create : 2020/04/21 テクノ古橋
        /// Update : 
        /// </remarks>
        //public async void Async_UpdateClimgingInfo(double latitude, double longitude, double altitude)
        public void UpdateClimgingInfo(double latitude, double longitude, double altitude, int dataUpdateInterval, int screenUpdateInterval)
        {
            //Console.WriteLine("call UpdateClimgingInfo");
            try
            {
                this._total_movingTime_sec += dataUpdateInterval;
                if (0 != _total_movingTime_sec % screenUpdateInterval)
                {
                    return;
                }
                var latitude_now = latitude;
                var longitude_now = longitude;
                var altitude_now = altitude;

                if (this._lastLatitude != latitude_now || this._lastLongitude != longitude_now || this._lastAltitude != altitude_now)
                {
                    if (_total_movingTime_sec < 2* screenUpdateInterval)
                    {
                        this._lastLatitude = latitude_now;
                        this._lastLongitude = longitude_now;
                        this._lastAltitude = altitude_now;
                    }

                    //Console.WriteLine(string.Format("latitude_now: {0}", latitude_now));
                    //Console.WriteLine(string.Format("longitude_now: {0}", longitude_now));
                    //Console.WriteLine(string.Format("altitude_now: {0}", altitude_now));
                    //Console.WriteLine(string.Format("this._lastLatitude: {0}", this._lastLatitude));
                    //Console.WriteLine(string.Format("this._lastLongitude: {0}", this._lastLongitude));
                    //Console.WriteLine(string.Format("this._lastAltitude: {0}", this._lastAltitude));

                    //値の更新（瞬間的な値）

                    //　垂直方向の移動距離算出  ★TODO:ここはEvaluationを利用することとする
                    this._distance_vertical = ClimbingAbilityCalculator.calcDistance_vertical(altitude_now, this._lastAltitude);

                    //水平方向の移動距離算出
                    this._distance_horizontal = ClimbingAbilityCalculator.calcDistance_Horizontal(latitude_now, longitude_now, this._lastLatitude, this._lastLongitude);

                    //登高速度の算出
                    this._speed_vertical = ClimbingAbilityCalculator.calcSpeed_vertical(this._distance_vertical, screenUpdateInterval);

                    //値の更新（累積的な値）
                    //水平方向の移動距離
                    if(this._distance_horizontal < 100.0)
                    {
                        this._total_movingDistance_meter += this._distance_horizontal;
                    }
                    //垂直方向の距離
                    if (0.0 < this._distance_vertical && this._distance_vertical < 100.0)
                    {
                        this._total_upMovingDistance_meter += this._distance_vertical;
                    }
                    else if ( -100.0 < this._distance_vertical && this._distance_vertical < 0.0)
                    {
                        this._total_downMovingDistance_meter += (-1 * this._distance_vertical);
                    }
                    else
                    {

                    }


                    //エネルギー消費量の算出
                    var work_rate = _cac.calcWorkRate(this._speed_vertical, this.person_weight, this.person_luggage);
                    //Console.WriteLine(string.Format("work_rate: {0}", work_rate));
                    //Console.WriteLine(string.Format("this._distance_horizontal: {0}",  this._total_upMovingDistance_meter));

                    this._energyConsumptionRate = _cac.calcEnergyConsumptionRate(work_rate);
                    this._total_energyConsumption = _cac.calcEnergyConsumption(
                                                                        this._total_movingTime_sec,
                                                                        this._total_movingDistance_meter,
                                                                        this._total_upMovingDistance_meter,
                                                                        this._total_downMovingDistance_meter,
                                                                        this.person_weight,
                                                                        this.person_luggage
                                                                        );

                    //運動強度の算出
                    this._METs_specific = _cac.calcMETs_specific(work_rate, this.person_weight);

                    this._METs_average = _cac.calcMETs_avarage(this._total_movingTime_sec, this._total_energyConsumption, this.person_weight);

                    //Console.WriteLine(string.Format("this._distance_vertical: {0}", this._distance_vertical));
                    //Console.WriteLine(string.Format("this._distance_horizontal: {0}", this._distance_horizontal));
                }

                //EventArgの更新
                if (Moving != null)
                {
                    MovingWatcherEventArgs args = new MovingWatcherEventArgs();

                    args.Latitude = latitude_now;
                    args.Longitude = longitude_now;
                    args.Altitude = altitude_now;

                    //瞬間的な値
                    args.Distance_vertical = this._distance_vertical;
                    args.Distance_horizontal = this._distance_horizontal;
                    args.Speed_vertical = this._speed_vertical;
                    args.EnergyConsumptionRate = this._energyConsumptionRate;
                    args.METs_specific = this._METs_specific;

                    //累積的な値
                    args.Total_movingTime_sec = this._total_movingTime_sec;
                    args.Total_movingDistance_meter = this._total_movingDistance_meter;
                    args.Total_upMovingDistance_meter = this._total_upMovingDistance_meter;
                    args.Total_downMovingDistance_meter = this._total_downMovingDistance_meter;
                    args.Total_energyConsumption = this._total_energyConsumption;
                    args.METs_average = this._METs_average;

                    Moving(this, args);
                }

                this._lastLatitude = latitude_now;
                this._lastLongitude = longitude_now;
                this._lastAltitude = altitude_now;

            }
            catch (Exception ex)
            {
                // Unable to get location
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }
        #endregion

        #region [Method]処理キューへの登高データの追加
        /// <summary>
        /// 処理キューへの登高データの追加
        /// </summary>
        /// <param name="val"></param>
        public void AddQueue(ClimbingData val)
        {
            ClimbingData newrec = new ClimbingData(val);
            _queueClimbing.Enqueue(newrec);
        }
        #endregion


    }

    #endregion

    public class MovingWatcherEventArgs : EventArgs
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public double Distance_vertical { get; set; }
        public double Distance_horizontal { get; set; }
        public double Speed_vertical  { get; set; }
        public double EnergyConsumptionRate  { get; set; }
        public double METs_specific  { get; set; }
        public double Total_movingTime_sec  { get; set; }
        public double Total_movingDistance_meter  { get; set; }
        public double Total_upMovingDistance_meter  { get; set; }
        public double Total_downMovingDistance_meter  { get; set; }
        public double Total_energyConsumption  { get; set; }
        public double METs_average  { get; set; }



        public MovingWatcherEventArgs()
        {
        }
    }


}