﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace TCLLIB.Sensors
{
    /// <summary>
    /// 気圧センサークラス
    /// </summary>
    /// <remarks>
    /// Description : 気圧センサーのインスタンスを保持
    /// Create : 2020/04/23 テクノ渡辺
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    class BarometerWrapper : SensorBase
    {
        #region プロパティ
        public bool IsMonitoring
        {
            get
            {
                bool result = false;
                try
                {
                    result = Barometer.IsMonitoring;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return result;
            }
        }
        #endregion

        #region イベントハンドラ

        /// <summary>
        /// デリゲート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnReadingChanged(object sender, BarometerReadingChangedEventArgs e);

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        public event EventHandler<BarometerReadingChangedEventArgs> ReadingChanged;

        #endregion

        public BarometerWrapper()
        {
            //イベントアサイン
            Barometer.ReadingChanged += Barometer_ReadingChanged;
        }

        public bool Start()
        {
            bool result = false;
            try
            {
                result = Start(base.speed);
            
            }catch(Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public bool Start(SensorSpeed speed)
        {
            bool result = false;
            try
            {
                if (!Barometer.IsMonitoring)
                {
                    Barometer.Start(speed);
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public bool Stop()
        {
            bool result = false;
            try
            {
                if (Barometer.IsMonitoring)
                {
                    Barometer.Stop();
                }
                result = true;

            }catch(Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #region [Event] 気圧計の値変化イベント
        /// <summary>
        /// 気圧計の値変化イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Barometer_ReadingChanged(object sender, BarometerChangedEventArgs e)
        {
            var data = e.Reading;
            if (ReadingChanged != null)
            {
                BarometerReadingChangedEventArgs args = new BarometerReadingChangedEventArgs(data.PressureInHectopascals);
                ReadingChanged(this, args);
            }
        }
        #endregion



        #region Dispose
        /// <summary>
        /// Dispose
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                //派生元をdispose
                base.Dispose(disposing);
            }catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }

    /// <summary>
    /// 気圧センサー値取得イベント用引数クラス
    /// </summary>
    /// <remarks>
    /// Description : イベントで取得した値またはエラー情報を格納するクラス
    /// Create : 2020/04/23 テクノ渡辺
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class BarometerReadingChangedEventArgs : EventArgs
    {
        public double PressureInHectopascals { get; set; }
        public BarometerReadingChangedEventArgs()
        {
        }
        public BarometerReadingChangedEventArgs(double hpa)
        {
            this.PressureInHectopascals = hpa;
        }
    }
}
