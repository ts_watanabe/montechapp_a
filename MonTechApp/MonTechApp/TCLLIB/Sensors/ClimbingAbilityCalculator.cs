﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using MonTechApp.Commons;

namespace TCLLIB.Sensors
{
    class ClimbingAbilityCalculator
    {
        #region [const] 定数値（固定）
        // エネルギー消費量
        public static readonly double ENAGEY_CONS_PARAM_H = 1.8;
        public static readonly double ENAGEY_CONS_PARAM_KM = 0.3;
        public static readonly double ENAGEY_CONS_PARAM_UP_KM = 10.0;
        public static readonly double ENAGEY_CONS_PARAM_DOWN_KM = 0.6;

        // エネルギー消費率（上り）
        public static readonly double UP_ENAGEY_CONSRATE_PARAM_1 = 0.012;
        public static readonly double UP_ENAGEY_CONSRATE_PARAM_2 = 1.67;
        // エネルギー消費率（下り）
        public static readonly double DOWN_ENAGEY_CONSRATE_PARAM_1 = 0.004;
        public static readonly double DOWN_ENAGEY_CONSRATE_PARAM_2 = 1.20;

        // METs（局所）（上り）
        public static readonly double METS_SPECIFIC_UP_PARAM_1 = 0.69;
        public static readonly double METS_SPECIFIC_UP_PARAM_2 = 95.2;
        // METs（局所）（下り）
        public static readonly double METS_SPECIFIC_DOWN_PARAM_1 = 0.23;
        public static readonly double METS_SPECIFIC_DOWN_PARAM_2 = 68.4;

        // METs（平均）
        public static readonly double AVERAGE_METS_PARAM = 0.95;
        #endregion

        //------------------------------------------------------------------------
        #region クラス変数
        //public double person_weight { get; set; }
        //public double person_luggage { get; set; }
        #endregion
        //------------------------------------------------------------------------
        #region コンストラクタ
        //public ClimbingAbilityCalculator(double weight_kg, double luggage_kg)
        //{
        //    this.person_weight = weight_kg;
        //    this.person_luggage = luggage_kg;
        //}
        public ClimbingAbilityCalculator()
        {

        }
        #endregion
        //------------------------------------------------------------------------


        #region [Method]  垂直方向の移動距離（m）算出 登高距離　★暫定実装
        /// <summary>
        ///     垂直方向の移動距離（登高距離） （m）を算出する　★符号ありとした
        /// </summary>
        /// <param name="altitude_new"></param>
        /// <param name="altitude_old"></param>
        /// <returns>
        ///     垂直方向の移動距離（m）    
        /// </returns>
        public static double calcDistance_vertical(double altitude_new, double altitude_old)
        {
            //TODO:下記は暫定
            return altitude_new - altitude_old;
        }
        #endregion

        #region [Method]  登高速度算出 ★暫定実装
        /// <summary>
        ///     登高速度（m/h）の算出 ★符号ありとした
        /// </summary>
        /// <param name="vertical_distance"></param>
        /// <param name="span_sec"></param>
        /// <returns>
        ///     登高速度（m/h）   
        /// </returns>
        public static double calcSpeed_vertical(double vertical_distance, int span_sec)
        {
            return vertical_distance / NumericalConversion.time_secToHour(span_sec);
        }
        #endregion

        #region [Method]  水平方向の移動距離（m）の算出 ★暫定実装
        /// <summary>
        ///      水平方向の移動距離（m）の算出する ★符号ありとした
        /// </summary>
        /// <param name="latitude_new"></param>
        /// <param name="longitude_new"></param>
        /// <param name="latitude_old"></param>
        /// <param name="longitude_old"></param>
        /// <returns>
        ///     水平方向の移動距離（m）  
        /// </returns>
        public static double calcDistance_Horizontal(double latitude_new, double longitude_new, double latitude_old, double longitude_old)
        {
            //TODO:下記は暫定
            var distance_km = Location.CalculateDistance(latitude_new, longitude_new, latitude_old, longitude_old, DistanceUnits.Kilometers);
            return NumericalConversion.distance_kmToMeter(distance_km);
        }
        #endregion


        #region メソッド

        //  仕事率(kg・m / 分)の算出  ★符号ありとした
        // 仕事率 = (体重＋ザックなどの総重量) * 垂直方向への移動速度
        public double calcWorkRate(double vertical_speed, double person_weight, double person_luggage)
        {
            //var speedPerMin = vertical_speed / RATE_HOUR_TO_MIN;
            return (person_weight + person_luggage) * (NumericalConversion.speed_perHourToPerMin(vertical_speed));
        }

        // エネルギー消費率の算出
        public double calcEnergyConsumptionRate(double work_rate)
        {
            double energyConsumptionRate = 0.0;
            if (0 < work_rate)
            {
                energyConsumptionRate = UP_ENAGEY_CONSRATE_PARAM_1 * work_rate + UP_ENAGEY_CONSRATE_PARAM_2;
            }
            else if (work_rate < 0)
            {
                work_rate = -1 * work_rate;
                energyConsumptionRate = DOWN_ENAGEY_CONSRATE_PARAM_1 * work_rate + DOWN_ENAGEY_CONSRATE_PARAM_2;
            }
            else
            {
                // TODO:平常時のエネルギー消費率でよいのか   平坦な道を歩く場合のエネルギー消費量は？
                energyConsumptionRate = 0.0;
            }
            return energyConsumptionRate;
        }

        // エネルギー消費量の算出 ★TODO:平常時のエネルギー消費量を考慮するひつようがあるかも
        // エネルギー消費率（上り）、エネルギー消費（下り）、累積のエネルギー消費量
        public double calcEnergyConsumption(double total_movingTime, double total_movingDistance, double total_up_movingDistance, double total_down_movingDistance, double person_weight, double person_luggage)
        {
            // 登山情報
            var climbingValue = person_weight + person_luggage;
            // コース定数
            var courseValue = ENAGEY_CONS_PARAM_H * (NumericalConversion.distance_meterToKm(total_movingTime))
                +ENAGEY_CONS_PARAM_KM * (NumericalConversion.distance_meterToKm(total_movingDistance))
                +ENAGEY_CONS_PARAM_UP_KM * (NumericalConversion.distance_meterToKm(total_up_movingDistance))
                + ENAGEY_CONS_PARAM_DOWN_KM * (NumericalConversion.distance_meterToKm(total_down_movingDistance));

            return climbingValue * courseValue;

        }

        //運動強度（局所）の算出
        public double calcMETs_specific(double work_rate, double person_weight)
        {
            double METs_specific = 0.0;
            if (0 < work_rate)
            {
                METs_specific = (METS_SPECIFIC_UP_PARAM_1 * work_rate + METS_SPECIFIC_UP_PARAM_2) / person_weight;
            }
            else if (work_rate < 0)
            {
                work_rate = -1 * work_rate;
                METs_specific = (METS_SPECIFIC_DOWN_PARAM_1 * work_rate + METS_SPECIFIC_DOWN_PARAM_2) / person_weight;
            }
            else
            {
                //TODO:平地の場合の運動強度 0.0 は正しいのか？
                METs_specific = 0.0;
            }
            return METs_specific;
        }

        //運動強度（累積に対する平均）の算出 ★TODO:時間変換時の端数はどうするのか（論文の理論に記述なし）
        public double calcMETs_avarage(double total_movingTime, double total_energy_consumption, double person_weight)
        {
            return total_energy_consumption * AVERAGE_METS_PARAM / person_weight / (NumericalConversion.time_secToHour(total_movingTime));
        }

        #endregion

    }
}
