﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace TCLLIB.Sensors
{
    /// <summary>
    /// センサー基本クラス
    /// </summary>
    /// <remarks>
    /// Description : 各種センサークラスの親クラス
    /// Create : 2020/04/23 テクノ渡辺
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    class SensorBase : IDisposable
    {

        public SensorSpeed speed = SensorSpeed.UI;

        /// <summary>
        /// Dispose済の判定フラグ
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// constructor
        /// </summary>
        public void Dispose()
        {
            try
            {
                Dispose(true);

                GC.SuppressFinalize(this);
            }catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        /// 派生クラスでOverrideされるDispposeメソッド
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    //終了処理があれば記述
                }

                _disposed = true;
            }
        }

        /// <summary>
        /// finalizer
        /// </summary>
        ~SensorBase()
        {
            Dispose(false);
        }
    }
}
