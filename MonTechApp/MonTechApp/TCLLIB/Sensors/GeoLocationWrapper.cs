﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;


namespace TCLLIB.Sensors
{
    class GeoLocationWrapper : SensorBase
    {
        #region [const] ズームレベル
        private const int ZOOM_LEVEL_MIN = 6;
        private const int ZOOM_LEVEL_MAX = 17;
        private const int ZOOM_LEVEL_ALTITUDE_MIN = 11;
        private const int ZOOM_LEVEL_ALTITUDE_MAX = 16;
        #endregion

        #region イベントハンドラ

        /// <summary>
        /// デリゲート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnLocationChanged(object sender, GeoLocationChangedEventArgs e);

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        public event EventHandler<GeoLocationChangedEventArgs> LocationChanged;

        #endregion

        private double _lastLatitude;
        private double _lastLongitude;
        private double _lastAccuracy;
        private double _lastWgsAltitude;

        private int _lastTileX;
        private int _lastTileY;

        public double Latitude
        {
            get
            {
                return _lastLatitude;
            }
        }
        public double Longitude
        {
            get
            {
                return _lastLongitude;
            }
        }

        public double Accuracy
        {
            get
            {
                return _lastAccuracy;
            }
        }

        public double WgsAltitude
        {
            get
            {
                return _lastWgsAltitude;
            }
        }

        //コンストラクタ
        public GeoLocationWrapper()
        {
            _lastLatitude = 0;
            _lastLongitude = 0;
            _lastAccuracy = 0;
            _lastWgsAltitude = 0;
            _lastTileX = 0;
            _lastTileY = 0;
        }

        #region [Method][async] GPSからの座標を取得し画面に表示する
        /// <summary>
        /// GetGeoLocation()
        /// </summary>
        /// <returns>
        ///   void
        /// </returns>
        /// <remarks>
        /// Description : GPSの値を取得し、画面内に表示する非同期メソッド
        /// Create : 2020/04/21 テクノ渡辺
        /// Update : 
        /// </remarks>
        public async void GetLocation()
        {
            try
            {
                //System.Diagnostics.Debug.WriteLine("GPSチェック中");
                //GPSからの値取得(1) ・・・ 現在のGPS値を取得する場合
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                var location = await Geolocation.GetLocationAsync(request);

                //var l = await Geolocation.GetLastKnownLocationAsync

                //GPSからの値取得(2) ・・・ 最後に参照した値を取得する場合
                //var location = await Geolocation.GetLastKnownLocationAsync();

                if (location != null)
                {
                    double acc = (location.Accuracy == null) ? 0 : location.Accuracy.Value;
                    double alt = (location.Altitude == null) ? 0 : location.Altitude.Value;

                    //System.Diagnostics.Debug.WriteLine("座標取得しました");
                    _lastLatitude = location.Latitude;
                    _lastLongitude = location.Longitude;
                    _lastAccuracy = acc;
                    _lastWgsAltitude = alt;

                    //緯度・経度からタイル番号を取得する
                    (int tx, int ty) center = GetTileIndex(location.Latitude, location.Longitude, ZOOM_LEVEL_ALTITUDE_MAX);
                    _lastTileX = center.tx;
                    _lastTileY = center.ty;
                
                    if (LocationChanged != null)
                    {
                        //System.Diagnostics.Debug.WriteLine("GPSイベント呼び出します");
                        GeoLocationChangedEventArgs arg = new GeoLocationChangedEventArgs();
                        arg.Latitude = _lastLatitude;
                        arg.Longitude = _lastLongitude;
                        arg.Accuracy = _lastAccuracy;
                        arg.AltitudeWGS = _lastWgsAltitude;
                        arg.TileX = _lastTileX;
                        arg.TileY = _lastTileY;
                        arg.TileZoom = ZOOM_LEVEL_ALTITUDE_MAX;
                        LocationChanged(this, arg);
                    }
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
            }
            catch (Exception ex)
            {
                // Unable to get location
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }
        #endregion

        #region [Method] GPSからの座標を基にタイル番号を取得する
        /// <summary>
        /// 緯度、経度、表示倍率をもとにタイル番号を求める
        /// </summary>
        /// <param name="lat">緯度</param>
        /// <param name="lon">経度</param>
        /// <param name="zoom">ズームレベル</param>
        /// <returns>
        ///     
        /// </returns>
        private (int tilex, int tiley) GetTileIndex(double lat, double lon, int zoom)
        {
            (int tilex, int tiley) result = (0, 0);
            double pi = Math.PI;
            double L = 85.05112878;
            try
            {
                double x = Math.Floor((Math.Pow(2, (zoom + 7))) * ((lon / 180) + 1));
                double y = Math.Floor((((Math.Pow(2, (zoom + 7))) / pi) * ((-1 * Atanh(Math.Sin((pi / 180) * lat))) + (Atanh(Math.Sin((pi / 180) * L))))));

                int tx = (int)Math.Floor(x / (double)256);
                int ty = (int)Math.Floor(y / (double)256);
                result = (tx, ty);
            }
            catch (Exception e)
            {
                string errmsg = "";
                throw e;
            }
            return result;
        }
        #endregion

        #region [Method] 引数の双曲線逆正接（ArcTanH）を求める
        /// <summary>
        /// 引数の双曲線逆正接（ArcTanH）を求める
        /// </summary>
        /// <param name="x"></param>
        /// <returns>
        ///     double
        /// </returns>
        private double Atanh(double x)
        {
            return (Math.Log((1 + x) / (1 - x)) / 2);
        }
        #endregion


    }

    /// <summary>
    /// GPS値取得イベント用引数クラス
    /// </summary>
    /// <remarks>
    /// Description : イベントで取得した値またはエラー情報を格納するクラス
    /// Create : 2020/04/23 テクノ渡辺
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class GeoLocationChangedEventArgs : EventArgs
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int TileX { get; set; }
        public int TileY { get; set; }
        public int TileZoom { get; set; }
        public double Accuracy { get; set; }
        public double AltitudeWGS { get; set; }

        public GeoLocationChangedEventArgs()
        {
        }
    }

}
