﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonTechApp.Exceptions
{
    class INVALID_INPUT_DATA : System.Exception
    {
        public INVALID_INPUT_DATA()
        {
        }

        public INVALID_INPUT_DATA(string message)
        : base(message)
        {
        }

        public INVALID_INPUT_DATA(string message, System.Exception inner)
        : base(message, inner)
        {
        }
    }
}
