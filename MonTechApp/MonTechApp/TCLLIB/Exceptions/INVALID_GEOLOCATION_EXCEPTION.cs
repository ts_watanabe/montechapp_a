﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MonTechApp.Exceptions
{
    class INVALID_GEOLOCATION_EXCEPTION : System.Exception
    {
        public INVALID_GEOLOCATION_EXCEPTION()
        {
        }
        public INVALID_GEOLOCATION_EXCEPTION(string message)
        : base(message)
        {
        }

        public INVALID_GEOLOCATION_EXCEPTION(string message, System.Exception inner)
        : base(message, inner)
        {
        }
    }
}
