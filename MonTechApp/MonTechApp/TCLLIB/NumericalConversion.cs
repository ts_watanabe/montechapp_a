﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TCLLIB
{
    public static class NumericalConversion
    {

        // １分（秒）
        public static readonly int RATE_MIN_TO_SEC = 60;
        // １時間（分）
        public static readonly int RATE_HOUR_TO_MIN = 60;
        // １時間（秒）
        public static readonly int RATE_HOUR_TO_SEC = 3600;

        // １KM（m）
        public static readonly int RATE_KM_TO_M = 1000;

        // 距離

        // km->meter　の変換
        public static double distance_kmToMeter(double distance_km)
        {
            return distance_km* RATE_KM_TO_M;
        }

        public static double　distance_meterToKm(double distance_meter)
        {
            return distance_meter / RATE_KM_TO_M;
        }

        // 速度

        public static double speed_perMinToPerHour(double speedPerMin)
        {
            return speedPerMin * RATE_HOUR_TO_MIN;
        }

        public static double speed_perHourToPerMin(double speedPerHour)
        {
            return speedPerHour / RATE_HOUR_TO_MIN;
        }

        // 時間
        public static double time_hourToMin(double time_hour)
        {
            return time_hour * RATE_HOUR_TO_MIN;
        }

        //★TODO：下記は調整が必要　端数の調整
        public static double time_minToHour(double time_min)
        {
            return time_min / RATE_HOUR_TO_MIN;
        }

        public static double time_hourToSec(double time_hour)
        {
            return time_hour * RATE_HOUR_TO_SEC;
        }

        //★TODO：下記は調整が必要　端数の調整
        public static double time_secToHour(double time_sec)
        {
            return time_sec / RATE_HOUR_TO_SEC;
        }
    }
}
