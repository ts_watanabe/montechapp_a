﻿using System;
using System.Collections.Generic;
using System.Text;
//using PCLStorage;
using Xamarin.Forms;
using System.Threading.Tasks;
//using MonTechApp.Sensors;
//using Xamarin.Essentials;
//using System.IO;
//using MonTechApp.Sensors;
using Xamarin.Forms.Internals;
using MonTechApp.Commons;
using TCLLIB.Sensors;
using MonTechApp.Interface;

namespace MonTechApp.Helper
{
    class FileHelper : HelperBase
    {
        #region クラス変数
        private IPlatformFile ipFile;
        //private IFolder rootFolder;
        private string rootFolder;
        private string subFolderName;
        private string fileName;
        //private IFolder subFolder;
        //private string rootFolder;
        //private SensorHelper sensorHelper;
        private List<ClimbingData> writeBuffer;
        #endregion

        #region プロパティ
        public string FilePath
        {
            get
            {
                //return string.Format("{0}/{1}/{2}", this.rootFolder.Path, this.subFolderName, this.fileName);
                return string.Format("{0}/{1}/{2}", this.rootFolder, this.subFolderName, this.fileName);

            }
        }
        #endregion

        #region コンストラクタ
        public FileHelper()
        {
            // ルートフォルダの取得
            ipFile = DependencyService.Get<IPlatformFile>(DependencyFetchTarget.GlobalInstance);

            //this.rootFolder = FileSystem.Current.LocalStorage;
            //this.rootFolder = FileSystem.Current.RoamingStorage;
            this.rootFolder = ipFile.GetRootFolder();
            this.subFolderName = ShareSetting.subFolderName;
            this.fileName = string.Format("clmbData_{0}.csv", DateTime.Now.ToString("yyyyMMddHHmm"));

            writeBuffer = new List<ClimbingData>();
            
        }
        #endregion

        #region メソッド

        public void SetWriteBuffer(List<ClimbingData> list)
        {
            foreach (ClimbingData row in list)
            {
                writeBuffer.Add(row);
                //string tmp = "{0} - {1},{2},{3}".Formats(row.rcd_tm, row.presnow, row.gpslat, row.gpslon);
                //System.Diagnostics.Debug.WriteLine(tmp);
            }
        }

        public async Task outPutData()
        {
            System.Diagnostics.Debug.WriteLine("出力前");
            //await outPutData(writeBuffer);
            System.Diagnostics.Debug.WriteLine("出力後");
            System.Diagnostics.Debug.WriteLine("バッファクリア");
            writeBuffer.Clear();
        }

        public string ConvertWriteLine(ClimbingData value)
        {
            string result = "";
            try
            {
                var strArray = new[] {
                    value.mtid.ToString(),
                    value.climbno.ToString(),
                    value.rcd_tm.ToString(),
                    value.gpslat.ToString(),
                    value.gpslon.ToString(),
                    value.gpsalt.ToString(),
                    value.wgsalt.ToString(),
                    value.gpsacc.ToString(),
                    value.gpsrgb.ToString(),
                    value.presnow.ToString(),
                    value.pressea.ToString(),
                    value.presalt.ToString(),
                    value.tilex.ToString(),
                    value.tiley.ToString(),
                    value.tilezoom.ToString(),
                    value.dst_vrt.ToString(),
                    value.dst_hrt.ToString(),
                    value.spd_vrt.ToString(),
                    value.ergcsn_rt.ToString(),
                    value.mets_sp.ToString(),
                    value.ttl_mvsec.ToString(),
                    value.ttl_mvdst.ToString(),
                    value.ttl_up_mvdst.ToString(),
                    value.ttl_dw_mvdst.ToString(),
                    value.ttl_ergcsn.ToString(),
                    value.mets_ave.ToString(),
                    value.psn_wgt.ToString(),
                    value.psn_lug.ToString(),
                    value.gpslon_crt.ToString(),
                    value.gpsalt_crt.ToString(),
                    value.wgsalt_crt.ToString(),
                };
                result = string.Join(",", strArray);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                throw ex;
            }
            return result;
        }

        #endregion
    }
}
