﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml.Xsl;
using MonTechApp;
using MonTechApp.Commons;
using MonTechApp.Exceptions;
using MonTechApp.Filters;

using SkiaSharp;
using System.Reflection;
using MonTechApp.Helper;
using MonTechApp.Interface;
using TCLLIB.Sensors;
using TCLLIB;

using FN = TCLLIB.Functions;
using Xamarin.Forms;
using System.Resources;
using System.Net.Http.Headers;
using Xamarin.Essentials;

namespace MonTechApp.Helper
{
    /// <summary>
    /// センサーヘルパークラス
    /// </summary>
    /// <remarks>
    /// Description : センサーからの入力、出力、監視等を一元管理するクラス
    /// Dependencies : 
    /// Create : 2020/04/30 テクノ渡辺
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class SensorHelper : HelperBase
    {
        #region イベントハンドラ
        /// <summary>
        /// デリゲート
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void OnMessageUpdate(object sender, SensorHelperEventArgs e);

        /// <summary>
        /// イベントハンドラ
        /// </summary>
        public event EventHandler<SensorHelperEventArgs> MessageUpdate;
        #endregion

        #region センサーラッパー用インスタンス
        private BarometerWrapper _barometer;
        private Elevation _elevation;
        private GeoLocationWrapper _location;
        private MovingWatcher _m_watcher;
        private FileHelper fileHelper;

        //private DetectShakeWrapper _shake;
        //private GyroscopeWrapper _gyro;
        //private MagnetometerWrapper _magnet;
        #endregion

        #region プロパティ
        /// <summary>
        /// 現時点の採取値
        /// </summary>
        public ClimbingData values { get; set; }

        public ClimbingData oldval { get; set; }

        public string OutPutFilePath
        {
            get
            {
                return this.fileHelper.FilePath;
            }
        }

        public bool IsPause { get; set; }

        #endregion


        #region その他クラス変数
        /// <summary>
        /// 直近のタイル番号(x)
        /// </summary>
        int lasttilex = 0;
        /// <summary>
        /// 直近のタイル番号(y)
        /// </summary>
        int lasttiley = 0;
        /// <summary>
        /// 直近の取得済み標高タイル
        /// </summary>
        SKBitmap bitmap;

        public List<ClimbingData> climbingBuffer;

        private ISleepControl isleepCnt;

        #endregion

        #region コンストラクタ

        public SensorHelper()
        {
            //Initialize
            _barometer = new BarometerWrapper();
            _elevation = new Elevation();
            _location = new GeoLocationWrapper();
            _m_watcher = new MovingWatcher();
            //_shake = new DetectShakeWrapper();
            //_gyro = new GyroscopeWrapper();
            //_magnet = new MagnetometerWrapper();

            values = new ClimbingData();
            climbingBuffer = new List<ClimbingData>();
            //var file = string.Format("clmbData_{0}.csv", DateTime.Now.ToString("yyyyMMddHHmm"));
            fileHelper = new FileHelper();

            bitmap = null;
            oldval = null;

            isleepCnt = DependencyService.Get<ISleepControl>(DependencyFetchTarget.GlobalInstance);

            _barometer.ReadingChanged += this.BarometerChanged;           
            _elevation.ElevatorChanged += this.ElevatorChanged;
            _location.LocationChanged += this.LocationUpdate;
            _m_watcher.Moving += this.ClimgingInfoUpdate;

            IsPause = false;
        }

        #endregion

        public void CreateClimbingData()
        {
            try
            {

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
            }
        }

        //開始
        public void Start()
        {
            // CPUのスリープ制御ON
            isleepCnt.SleepDisabled();
            //_barometer.Start();
            //_elevation.Start();

            if (!IsPause)
            {
                _m_watcher.person_weight = values.psn_wgt;
                _m_watcher.person_luggage = values.psn_lug;

                var geoLocator = DependencyService.Get<IGeoLocator>();
                geoLocator.LocationReceived += (_, args) =>
                {
                    //lblHelperMessage.Text = "Lat={0}, Lon={1}".Formats(args.Latitude, args.Longitude);
                    values.gpslat = args.Latitude;
                    values.gpslon = args.Longitude;
                    values.gpsacc = args.Accuracy;
                    values.gpsalt = args.Altitude;
                };
                geoLocator.StartGps();

                //タイマーによるセンサー値取得イベント
                Xamarin.Forms.Device.StartTimer(TimeSpan.FromSeconds(AppResources.DEFAULT_SENSORTIMER_INTERVAL.ToInt()), SensorWatchTimerJob);
            }

            IsPause = false;
        }

        //中断
        public void Pause()
        {
            //CPUのスリープ制限解除
            isleepCnt.SleepEnabled();
            //_elevation.Stop();

            IsPause = true;

        }

        //終了
        public void Stop()
        {
            IsPause = true;
        }

        void BarometerChanged(object sender, BarometerReadingChangedEventArgs e)
        {
            try
            {
                //取得気圧の更新
                this.values.presnow = e.PressureInHectopascals;
            }
            catch (Exception ex)
            {
                UpdateMessage(string.Format("気圧取得で例外発生 {0}", ex.Message));
            }
        }

        #region [Event] 標高更新イベント
        /// <summary>
        /// 標高更新イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ElevatorChanged(object sender, ElevetorChangedEventArgs e)
        {
            try
            {
                //取得気圧の更新
                this.values.presnow = e.PressureInHectopascals;
                this.values.presalt = e.Elevator;
            }
            catch (Exception ex)
            {
                UpdateMessage(string.Format("気圧標高取得で例外発生 {0}", ex.Message));
            }
        }
        #endregion

        #region [Event] GPS座標更新イベント
        void LocationUpdate(object sendeUpdateClimgingInfor, GeoLocationChangedEventArgs e)
        {
            try
            {
                //現在時刻の取得
                //this.values.rcd_tm = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                //Console.WriteLine(string.Format("LocationUpdate:{0}", this.values.rcd_tm));

                //緯度・経度の表示更新
                this.values.gpslat = e.Latitude;
                this.values.gpslon = e.Longitude;
                this.values.tilex = e.TileX;
                this.values.tiley = e.TileY;
                this.values.tilezoom = e.TileZoom;
                this.values.gpsacc = e.Accuracy;
                this.values.wgsalt = e.AltitudeWGS;

                (byte red, byte green, byte blue) = GetPixcelFromTile(e.Latitude, e.Longitude);
                this.values.gpsrgb = "{0:x2}{1:x2}{2:x2}".Formats(red, green, blue);

                this.values.gpsalt = GetElevationFromPicture(e.Latitude, e.Longitude);
                //this.values.gpsalt = ConvertRGBToAltitude(red, green, blue);

                //UpdateMessage("標高={0}".Formats(this.values.gpsalt));


            }
            catch (Exception ex)
            {
                UpdateMessage(string.Format("GPS座標取得で例外発生 {0}", ex.Message));
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
        #endregion

        #region [Event] 登山データ更新イベント
        void ClimgingInfoUpdate(object sender, MovingWatcherEventArgs e)
        {
            //Console.WriteLine("call ClimgingInfoUpdate");
            try
            {
                //瞬間値
                this.values.dst_vrt = e.Distance_vertical;
                //this.values.dst_hrt = e.Distance_horizontal;  水平距離は1秒単位で算出する
                this.values.spd_vrt = e.Speed_vertical;
                this.values.ergcsn_rt = e.EnergyConsumptionRate;
                this.values.mets_sp = e.METs_specific;

                //累積値
                this.values.ttl_mvsec = e.Total_movingTime_sec;
                this.values.ttl_mvdst = e.Total_movingDistance_meter;
                this.values.ttl_up_mvdst = e.Total_upMovingDistance_meter;
                this.values.ttl_dw_mvdst = e.Total_downMovingDistance_meter;
                this.values.ttl_ergcsn = e.Total_energyConsumption;
                this.values.mets_ave = e.METs_average;
            }
            catch (Exception ex)
            {
                UpdateMessage(string.Format("登山データ更新で例外発生 {0}", ex.Message));
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
        #endregion


        #region [Method] タイマーイベントによる座標取得コールバックメソッド 
        /// <summary>
        /// タイマーイベント用コールバックメソッド
        /// </summary>
        /// <returns>
        /// true:イベント継続、false:イベント終了
        /// </returns>
        private bool SensorWatchTimerJob()
        {
            bool result = true;

            //センサー値記録日時を更新
            this.values.rcd_tm = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.fff");

            //座標取得
            //_location.GetLocation();

            //水平方向の移動距離の更新（1秒）
            if (null != oldval)
            {
                this.values.dst_hrt = ClimbingAbilityCalculator.calcDistance_Horizontal(_location.Latitude, _location.Longitude, this.oldval.gpslat, this.oldval.gpslon);
            }

            //一つ前の値を保持
            oldval = new ClimbingData(values);

            //値の更新  TODO：現時点では10秒ごととした
            //_m_watcher.UpdateClimgingInfo(this.values.gpslat, this.values.gpslon, this.values.gpsalt, AppResources.DEFAULT_DATAUPDATE_INTERVAL.ToInt(), AppResources.DEFAULT_SCREENUPDATE_INTERVAL.ToInt());

            //GPS Accuracy が 閾値以上であれば実施する処理（０なら毎回更新）
            double accThreshold = AppResources.DEFAULT_ACCURACY_THRESHOLD.ToDouble();
            if ((accThreshold == 0) || (values.gpsacc >= accThreshold))
            {
                _m_watcher.UpdateClimgingInfo(this.values.gpslat, this.values.gpslon, this.values.gpsalt, AppResources.DEFAULT_SENSORTIMER_INTERVAL.ToInt(), AppResources.DEFAULT_SCREENUPDATE_INTERVAL.ToInt());
            }

            //センサー値の取得を実装元に知らせるイベント
            if (MessageUpdate != null)
            {
                MessageUpdate(this, new SensorHelperEventArgs());
            }

            //メッセンジャーから送信
            MessagingCenter.Send<SensorHelper>(this, "Update");

            //一時停止中ならここで戻る
            if (IsPause)
            {
                return true;
            }

            //データ出力
            try
            {
                var pathname = Preferences.Get(AppResources.PRF_KEY_SAVEFILE, "");
                if (pathname.IsNullOrEmpty())
                {
                    throw new Exception("undefined save file.");
                }

                IPlatformFile ipFile = DependencyService.Get<IPlatformFile>(DependencyFetchTarget.GlobalInstance);
                ipFile.AppendLine(pathname, fileHelper.ConvertWriteLine(this.values));

                result = true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                result = false;
            }

            if (IsPause)
            {
                result = false;
            }

            return result;
        }
        #endregion

        #region [Method] 登高データの保存バッファへの追加
        /// <summary>
        /// データ保存バッファへの追加
        /// </summary>
        /// <param name="rec"></param>
        public void AppendClimbingWriteBuffer(ClimbingData rec)
        {
            ClimbingData newrec = new ClimbingData(rec);
            this.climbingBuffer.Add(newrec);
        }
        #endregion

        #region [Method] 標高タイルのRGB値取得
        public (byte R, byte G, byte B) GetPixcelFromTile(double lat, double lon)
        {
            (byte R, byte G, byte B) result = ((byte)0, (byte)0, (byte)0);
            try
            {
                //緯度・経度からタイル番号を取得
                (int tx, int ty) = FN.GetTileIndex(lat, lon, AppResources.ELEVATION_ZOOM_MAX.ToInt());

                bool getbitmap = false;

                //初回のタイル参照かタイル番号の変化で標高タイルを読み込む
                if (lasttilex != tx || lasttiley != ty || bitmap == null)
                {
                    //タイルのファイル名を作成
                    string filename = "{0}_{1}_{2}.png".Formats(AppResources.ELEVATION_ZOOM_MAX.ToInt(), tx, ty);
                    string fullname = this.GetType().Assembly.Location;
                    string assemblyname = System.IO.Path.GetFileNameWithoutExtension(fullname);
                    string resourceid = "{0}.{1}.{2}".Formats(assemblyname, "tiles", filename);

                    bool doesExists = File.Exists(resourceid);

                    lasttilex = tx;
                    lasttiley = ty;

                    bitmap = SKBitmap.Decode(GetTileResource(tx, ty));

                    if (bitmap.ByteCount > 0)
                    {
                        getbitmap = true;
                    }
                }
                else
                {
                    if (bitmap != null)
                    {
                        getbitmap = true;
                    }
                }
                if (getbitmap)
                {
                    //緯度・経度を標高タイル内のピクセル座標に変換する
                    (double px, double py) = FN.GetPositionInTile(lat, lon, AppResources.ELEVATION_ZOOM_MAX.ToInt());

                    //ピクセル座標の画素情報を取得する
                    SKColor pix = bitmap.GetPixel((int)px, (int)py);

                    result.R = pix.Red;
                    result.G = pix.Green;
                    result.B = pix.Blue;
                }
            }
            catch (INVALID_GEOLOCATION_EXCEPTION ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                string errmsg = MsgResources.ERRMSG_010;
                UpdateMessage(errmsg, AppResources.HELPER_STATUS_ERROR.ToInt());
            }
            catch (Exception ex)
            {
                UpdateMessage(ex.Message, AppResources.HELPER_STATUS_ERROR.ToInt());
            }


            return result;
        }
        #endregion

        #region [Method] RGB値を標高値に変換
        public double ConvertRGBToAltitude(byte red, byte blue, byte green)
        {
            double result = 0;
            try
            {
                //画素情報から標高を算出する
                result = FN.ComputeElevator((int)red, (int)green, (int)blue);
            }
            catch (Exception ex)
            {
                UpdateMessage(ex.Message, AppResources.HELPER_STATUS_ERROR.ToInt());
            }
            return result;
        }
        #endregion


        #region [Method] 標高タイルからの標高算出
        /// <summary>
        /// 標高タイルからの標高算出
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <returns></returns>
        public double GetElevationFromPicture(double lat, double lon)
        {
            double result = 0;

            try
            {
                //緯度・経度からタイル番号を取得
                (int tx, int ty) = FN.GetTileIndex(lat, lon, AppResources.ELEVATION_ZOOM_MAX.ToInt());

                bool getbitmap = false;

                //初回のタイル参照かタイル番号の変化で標高タイルを読み込む
                if (lasttilex != tx || lasttiley != ty || bitmap == null)
                {
                    //タイルのファイル名を作成
                    string filename = "{0}_{1}_{2}.png".Formats(AppResources.ELEVATION_ZOOM_MAX.ToInt(), tx, ty);
                    string fullname = this.GetType().Assembly.Location;
                    string assemblyname = System.IO.Path.GetFileNameWithoutExtension(fullname);
                    string resourceid = "{0}.{1}.{2}".Formats(assemblyname, "tiles", filename);

                    bool doesExists = File.Exists(resourceid);

                    lasttilex = tx;
                    lasttiley = ty;

                    bitmap = SKBitmap.Decode(GetTileResource(tx, ty));

                    if (bitmap.ByteCount > 0)
                    {
                        getbitmap = true;
                    }
                }
                else
                {
                    if (bitmap != null)
                    {
                        getbitmap = true;
                    }
                }
                double alt = 0;
                if (getbitmap)
                {
                    //緯度・経度を標高タイル内のピクセル座標に変換する
                    (double px, double py) = FN.GetPositionInTile(lat, lon, AppResources.ELEVATION_ZOOM_MAX.ToInt());

                    //ピクセル座標の画素情報を取得する
                    SKColor pix = bitmap.GetPixel((int)px, (int)py);

                    int red = (int)pix.Red;
                    int green = (int)pix.Green;
                    int blue = (int)pix.Blue;

                    //画素情報から標高を算出する
                    alt = FN.ComputeElevator(red, green, blue);
                }
                result = alt;
            }
            catch(INVALID_GEOLOCATION_EXCEPTION ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                string errmsg = MsgResources.ERRMSG_010;
                UpdateMessage(errmsg, AppResources.HELPER_STATUS_ERROR.ToInt());
            }
            catch (Exception ex)
            {
                UpdateMessage(ex.Message, AppResources.HELPER_STATUS_ERROR.ToInt());
            }
            return result;
        }
        #endregion

        #region [Method] タイル座標を指定して埋め込みリソースを取得する
        /// <summary>
        /// タイル座標を指定して埋め込みリソースを取得する
        /// </summary>
        /// <param name="tilex"></param>
        /// <param name="tiley"></param>
        /// <returns></returns>
        private byte[] GetTileResource(int tilex, int tiley)
        {
            byte[] result = new byte[] { };

#if ZZZ
            switch (tilex)
            {
                case 29049:
                    //神奈川県 陣場山 x-1/3
                    switch (tiley)
                    {
                        case 12906:
                            result = AppResources._15_29049_12906;
                            break;
                        case 12907:
                            result = AppResources._15_29049_12907;
                            break;
                        case 12908:
                            result = AppResources._15_29049_12908;
                            break;
                        case 12909:
                            result = AppResources._15_29049_12909;
                            break;
                        case 12910:
                            result = AppResources._15_29049_12910;
                            break;
                    }
                    break;
                case 29050:
                    //神奈川県 陣場山 x-2/3
                    switch (tiley)
                    {
                        case 12906:
                            result = AppResources._15_29050_12906;
                            break;
                        case 12907:
                            result = AppResources._15_29050_12907;
                            break;
                        case 12908:
                            result = AppResources._15_29050_12908;
                            break;
                        case 12909:
                            result = AppResources._15_29050_12909;
                            break;
                        case 12910:
                            result = AppResources._15_29050_12910;
                            break;
                    }
                    break;
                case 29051:
                    //神奈川県 陣場山 x-3/3
                    switch (tiley)
                    {
                        case 12906:
                            result = AppResources._15_29051_12906;
                            break;
                        case 12907:
                            result = AppResources._15_29051_12907;
                            break;
                        case 12908:
                            result = AppResources._15_29051_12908;
                            break;
                        case 12909:
                            result = AppResources._15_29051_12909;
                            break;
                        case 12910:
                            result = AppResources._15_29051_12910;
                            break;
                    }
                    break;
                case 29092:
                    //坂口宅　x-1/2
                    switch (tiley)
                    {
                        case 12921:
                            result = AppResources._15_29092_12921;
                            break;
                    }
                    break;
                case 29093:
                    //坂口宅 x-2/2
                    switch (tiley)
                    {
                        case 12921:
                            result = AppResources._15_29093_12921;
                            break;
                    }
                    break;
                case 29099:
                    //栃木県 太平山 x-1/3
                    switch (tiley)
                    {
                        case 12824:
                            result = AppResources._15_29099_12824;
                            break;
                        case 12825:
                            result = AppResources._15_29099_12825;
                            break;
                        case 12826:
                            result = AppResources._15_29099_12826;
                            break;
                        case 12827:
                            result = AppResources._15_29099_12827;
                            break;
                    }
                    break;
                case 29100:
                    //栃木県 太平山 x-2/3
                    switch (tiley)
                    {
                        case 12824:
                            result = AppResources._15_29100_12824;
                            break;
                        case 12825:
                            result = AppResources._15_29100_12825;
                            break;
                        case 12826:
                            result = AppResources._15_29100_12826;
                            break;
                        case 12827:
                            result = AppResources._15_29100_12827;
                            break;
                    }
                    break;
                case 29101:
                    //栃木県 太平山 x-3/3
                    //渡辺宅
                    switch (tiley)
                    {
                        case 12824:
                            result = AppResources._15_29101_12824;
                            break;
                        case 12825:
                            result = AppResources._15_29101_12825;
                            break;
                        case 12826:
                            result = AppResources._15_29101_12826;
                            break;
                    }
                    break;
                case 29073:
                    //古橋宅
                    switch (tiley)
                    {
                        case 12927:
                            result = AppResources._15_29073_12927;
                            break;
                    }
                    break;
                case 28361:
                    //田中卓
                    switch (tiley)
                    {
                        case 13083:
                            result = AppResources._15_28361_13083;
                            break;
                    }
                    break;
                case 29106:
                    //テクノソリューションオフィス x-1/2
                    switch (tiley)
                    {
                        case 12903:
                            result = AppResources._15_29106_12903;
                            break;
                        case 12904:
                            result = AppResources._15_29106_12904;
                            break;
                    }
                    break;
                case 29107:
                    //テクノソリューションオフィス x-2/2
                    switch (tiley)
                    {
                        case 12903:
                            result = AppResources._15_29107_12903;
                            break;
                        case 12904:
                            result = AppResources._15_29107_12904;
                            break;
                    }
                    break;
            }
#endif
            return result;
        }
#endregion

        //実装元にメッセージイベントを発行する
        private void UpdateMessage(string message)
        {
            if (MessageUpdate != null)
            {
                SensorHelperEventArgs arg = new SensorHelperEventArgs
                {
                    LastMessage = message,
                    Status = 0,
                };
                MessageUpdate(this, arg);
            }
        }

        private void UpdateMessage(string message, int status)
        {
            if (MessageUpdate != null)
            {
                SensorHelperEventArgs arg = new SensorHelperEventArgs
                {
                    LastMessage = message,
                    Status = status,
                };
                MessageUpdate(this, arg);
            }
        }


    }

    /// <summary>
    /// 標高値取得イベント用引数クラス
    /// </summary>
    /// <remarks>
    /// Description : イベントで取得した値またはエラー情報を格納するクラス
    /// Create : 2020/04/23 テクノ渡辺
    /// Update :
    /// Copyright © Technosolution Co., Ltd. All rights reserved 
    /// </remarks>
    /// <example>
    /// <code>
    /// </code>
    /// </example>
    public class SensorHelperEventArgs : EventArgs
    {
        public string LastMessage { get; set; }

        public int Status { get; set; }
        public SensorHelperEventArgs()
        {
        }
    }

}
