﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

using MonTechApp.Interface;
using CoreLocation;

[assembly: Xamarin.Forms.Dependency(typeof(MonTechApp.iOS.GeoLocation_iOS))]


namespace MonTechApp.iOS
{
    public class GeoLocation_iOS : IGeoLocator
    {
        public event LocationEventHandler LocationReceived;

        private readonly CLLocationManager _locationMan = new CLLocationManager();

        public void StartGps()
        {
            _locationMan.LocationsUpdated += (sender, e) =>
            {
                if (this.LocationReceived != null)
                {
                    var l = e.Locations[e.Locations.Length - 1];

                    this.LocationReceived(this, new LocationEventArgs
                    {
                        Latitude = l.Coordinate.Latitude,
                        Longitude = l.Coordinate.Longitude
                    });
                }
            };
            _locationMan.StartUpdatingLocation();
        }
    }
}